const MongoClient = require("mongodb").MongoClient;
const connString = "mongodb+srv://dbnikhil:pass@123/test?retryWrites=true&w=majority";
const dbName = "SoftwareDownloadServie";

async function initialize(
    collectionName: string,
) {
    try {
        const client = new MongoClient(connString, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        await client.connect();
        const collection = await client.db(dbName).collection(collectionName);
        console.log("[MongoDB connection] SUCCESS");

        // perform actions on the collection object
        client.close();
        console.log("connection closed");
        return collection;
    } catch (err: any) {
        console.log("[MongoDB connection] ERROR: " + err);
        return err;
    }
}

async function getData(collectionName: string) {
    try {
        const response = await initialize(collectionName);
        const result = await dbCollection.find().toArray();
        return JSON.stringify(result);
    } catch (err) {
        return err;
    }
}

export const methods = {
    initialize,
    getData
};

export default methods;