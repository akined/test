'use strict';
const AWS = require('aws-sdk');
const stepFunctions = new AWS.StepFunctions({});
const lambda = new AWS.Lambda({});
const messagingHistory = process.env.MESSAGING_HISTORY_LAMBDA;
const stepFn = process.env.STEP_FUNCTION;
process.env.AWS_NODEJS_CONNECTION_REUSE_ENABLED = 1;
const fs = require('fs');
const skillId = process.env.SKILLID

const close = (fulfillment_state, message, session_attributes = {}) => {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': {
                'contentType': 'PlainText',
                'content': message
            }
        }
    };
    console.log("===========close dialoge response============", dialogResponse);
    return dialogResponse;
};

const elicitSlot = (sessionAttributes, intentName, slots, slotToElicit, message) => {
    const dialogResponse = {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,
        },
    };
    console.log("===========elicitSlot dialoge response============", dialogResponse);
    return dialogResponse;
};

async function confirmIntent(intentName, message, session_attributes = {}) {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intentName,
            'message': {
                'contentType': 'CustomPayload',
                'content': message
            },
            'responseCard': {
                'version': 1,
                'contentType': 'application/vnd.amazonaws.card.generic',
                'genericAttachments': [{
                    // 'title': 'Do you need anything else for this customer',
                    'buttons': [
                        { text: 'Yes', value: 'Yes' },
                        { text: 'No', value: 'No' },
                    ]
                }]
            }
        }
    };
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}

async function invokeLambda(inputData) {
    try {
        const params = {
            FunctionName: messagingHistory,
            Payload: JSON.stringify({ conversationId: inputData })
        };
        console.log("=========params=========", JSON.stringify(params));
        const output = await lambda.invoke(params).promise();
        console.log("=======invokeLambda========", JSON.stringify(output));
        return output;
    } catch (err) {
        console.log("Error While Executing lambda Function", err);
        return err;
    }
}

// ---------------------------------------------------- Setting up Step function connection. ------------------------------------------ //
//this function will get the output from step function
let counter = 0;

async function delay(ms) {
    // return await for better async stack trace support in case of errors.
    return await new Promise(resolve => setTimeout(resolve, ms));
}

async function describeStepExecution(executionResult) {
    console.log("========executionResult=============", executionResult);
    const activityTaskReq = {
        executionArn: executionResult["executionArn"],
    };

    const stepFnResponse = await stepFunctions.describeExecution(activityTaskReq).promise();
    console.log("========stepFnResponse=============", JSON.stringify(stepFnResponse));
    if (stepFnResponse["status"] == "RUNNING" && counter < 12) {
        counter++;
        await delay(1000);
        return await describeStepExecution(executionResult);
    } else {
        return stepFnResponse;
    }
}

//this function will start the step function
async function startStepFnExecution(inputData) {
    try {
        const params = {
            stateMachineArn: stepFn,
            input: JSON.stringify({ birthDate: inputData.dateOfBirth, phone: inputData.phoneNumber }),
        };
        //startinghere step function
        const startExecution = await stepFunctions.startExecution(params).promise();
        console.log("========executionResult=============", startExecution);
        const describeStepExecutionResponse = await describeStepExecution(startExecution);
        console.log("counter", counter);
        counter = 0;
        console.log(" counter after", counter);
        console.log("========describeStepExecutionResponse=============", JSON.stringify(describeStepExecutionResponse));
        const finalResponse = {
            status: true,
            data: describeStepExecutionResponse
        };
        return finalResponse;
    } catch (err) {
        console.log("Error While Executing Step Function", err);
        const finalResponse = {
            status: false,
            data: err
        };
        return finalResponse;
    }
}

//This function will start execution
async function executeStateMachine(intent_request, session_attributes) {
    const slots = intent_request['currentIntent']['slots'];
    console.log("sepearate attribute", session_attributes);
    const sfppUser = session_attributes.sfppUser;
    const customerInfo = JSON.parse(session_attributes.customerInfo);
    const message = `Thank you, I am connecting you to a representative with following details \n
                    Phone Number: ${customerInfo["phoneNumber"]},\n
                    Birth Date: ${customerInfo["dateOfBirth"]}`;
    const stepFnResponse = await startStepFnExecution(slots);

    if (stepFnResponse["status"] && stepFnResponse["data"]["status"] == "SUCCEEDED") {
        const finalStepFn = JSON.parse(stepFnResponse["data"]["output"]);
        console.log("finalStepFn", JSON.stringify(finalStepFn));
        if (typeof finalStepFn == "string") {
            const sessionAttributes = {
                sfppUser, customerInfo: session_attributes.customerInfo, adapterAction: "TransferWithMessage", skillId: skillId
            };
            return close("Fulfilled", message, sessionAttributes);
        } else {
            const mailingAddr = `${finalStepFn['MailingAddress']['addressLines']} ${finalStepFn['MailingAddress']['city']} 
            ${finalStepFn['MailingAddress']['stateProvince']} ${finalStepFn['MailingAddress']['postalCode']}`;
            const customerName = `${finalStepFn['Name']['firstName']} ${finalStepFn['Name']['middleName']} ${finalStepFn['Name']['lastName']}`;
            const customerPhoneNumber = customerInfo["phoneNumber"];
            const DOB = customerInfo["dateOfBirth"];

            const accounts = finalStepFn.Policies.map(obj => obj.plcyAcctNum);
            const lobData = finalStepFn.Policies[0]['lob'];

            console.log("accounts data", policies);
            const message = `Thank you, I am connecting you to a representative with following details \n
            Customer Name : ${customerName}\n
            Address : ${mailingAddr}\n
            Policies: ${accounts.toString()}\n
            Phone Number : ${customerPhoneNumber}\n
            Date of Birth : ${DOB}`;
            let session_attributes = {
                Policies: JSON.stringify(finalStepFn),
                adapterAction: (lobData == "H" || lobData == "L")
                    ? 2117648330 : 2117648330,
                sfppUser,
                customerInfo: session_attributes.customerInfo,
            };
            return close("Fulfilled", message, session_attributes);
        }
    } else {
        const session_attributes = {
            sfppUser, customerInfo: session_attributes.customerInfo, adapterAction: 'TransferWithMessage', skillId
        };
        return close('Fulfilled', message, session_attributes);
    }
}
async function initiate(intentName, session_attributes) {
    const dialogResponse = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': {
                'contentType': 'CustomPayload',
                'content': "What is your topic?"
            },
            'responseCard': {
                'version': 1,
                'contentType': 'application/vnd.amazonaws.card.generic',
                'genericAttachments': [{
                    'buttons': [
                        { text: 'Re-Bill', value: 'Re-Bill' },
                        { text: 'Combine Account', value: 'Combine Account' },
                        { text: 'Bill Through Divisions', value: 'Bill Through Divisions' },
                        { text: 'Change Amount Due Date', value: 'Change Amount Due Date' },
                        { text: 'Suspending Automated Draft', value: 'Suspending Automated Draft' }
                    ]
                },
                {
                    'buttons': [
                        { text: 'Reinstate Automated Draft', value: 'Reinstate Automated Draft' },
                        { text: 'Reviewing Current Account History', value: 'Reviewing Current Account History' },
                        { text: 'All Other Topics', value: 'All Other Topics' }
                    ]
                }
                ]
            }
        }
    };
    console.log("================ dialogResponse===========", JSON.stringify(dialogResponse));
    return dialogResponse;
}

async function executeAfterValidation(intent_request) {
    const phonenumber = intent_request['currentIntent']['slots']['phoneNumber'];
    const dateofbirth = intent_request['currentIntent']['slots']['dateOfBirth'];
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request['sessionAttributes'];
    const sfppUser = session_attributes.sfppUser;
    console.log("inside the phonenumber and dob is not null part");
    const filteredNumber = phonenumber.replace(/[^\d]/g, '');
    console.log("filteredNumber", filteredNumber);
    if (filteredNumber.length == 10) {
        console.log("=======filteredNumber length is 10==========", filteredNumber);
        intent_request['currentIntent']['slots']['phoneNumber'] = filteredNumber;
    } else {
        console.log("=======filteredNumber length is > 10==========", filteredNumber);
        const finalNumber = filteredNumber.substr(1);
        intent_request['currentIntent']['slots']['phoneNumber'] = finalNumber;
    }
    console.log("phonenumber", intent_request['currentIntent']['slots']['phoneNumber']);
    const customerInfo = JSON.stringify({
        phoneNumber: phonenumber,
        dateOfBirth: dateofbirth,
    });

    if (intentName == 'PCU_SFPP_Billing_AllOtherTopics') {
        console.log("All other topics selected");
        const session_attributes = { sfppUser, customerInfo };
        return await executeStateMachine(intent_request, session_attributes);
    }
    else {
        // intent_request["sessionAttributes"] = session_attributes;
        const session_attributes = { sfppUser, customerInfo };
        return await getResponse(intent_request, session_attributes);
    }
}

async function slotsValidationFailMessage(intent_request, slotKey) {
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    const slots = intent_request['currentIntent']['slots'];
    console.log("inside the slotsValidationFailMessage null part", intent_request['currentIntent']['slots']);
    return elicitSlot(session_attributes, intentName, slots, slotKey);
}

async function validateSlotsAndExecute(intent_request) {
    const phonenumber = intent_request['currentIntent']['slots']['phoneNumber'];
    const dateofbirth = intent_request['currentIntent']['slots']['dateOfBirth'];
    if (phonenumber != null && dateofbirth != null) {
        return await executeAfterValidation(intent_request);
    } else if (phonenumber != null) {
        return await slotsValidationFailMessage(intent_request, 'dateOfBirth');
    } else if (phonenumber == null) {
        return await slotsValidationFailMessage(intent_request, 'phoneNumber');
    }
}

async function customerData(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];

    if (confirmationStatus == 'None') {
        console.log("=========inside confirmationStatus none==================");
        return await validateSlotsAndExecute(intent_request);
    }
    else if (confirmationStatus == 'Denied') {
        const message = `Thank you, you can now close the chat window.`;
        return close('Fulfilled', message, session_attributes);
    }
    else if (confirmationStatus == 'Confirmed') {
        intent_request['currentIntent']['slots'] = session_attributes;
        return await executeStateMachine(intent_request, session_attributes);
    }
}

async function initialResponse(intent_request) {
    const intentName = intent_request['currentIntent']['name'];
    console.log("name", intentName);
    let session_attributes = intent_request["sessionAttributes"];
    let convID = "75bf8441-1979-44e7-ae3c-237b886ec35a";

    let retStateCode = await invokeLambda(convID);
    console.log("messaging history details", retStateCode);

    //if session attributes recieved null, then reset to empty.
    if (session_attributes == null) {
        session_attributes = {};
    }

    const data = JSON.parse(retStateCode.Payload);
    console.log("data", data);


    if (retStateCode.StatusCode == 200 && data.status == 200) {
        console.log("in if block");
        session_attributes['sfppUser'] = JSON.stringify(data.data);
        return initiate(intentName, session_attributes);
    } else {
        console.log("in else block");
        return initiate(intentName, session_attributes);
    }
    // session_attributes['rtnButtons'] = JSON.stringify(rtMetaData.buttons);

}

async function getResponse(intent_request, session_attributes) {
    const intentName = intent_request['currentIntent']['name'];
    // const session_attributes = intent_request["sessionAttributes"];
    const responsesData = fs.readFileSync('./responses.json').toString();
    const responses = JSON.parse(responsesData);

    console.log("responses===================================", responses);
    const intentNameResponse = responses.filter(data => data.IntentName == intentName);
    console.log("responsintentNameResponse=========================================", intentNameResponse);
    if (Array.isArray(intentNameResponse) && intentNameResponse.length > 0) {
        const generalAttachment = '\n\n  Is there anything else I can help you with today?';
        const message = intentNameResponse[0].Response + generalAttachment;
        console.log("response", message);
        return confirmIntent(intentName, message, session_attributes);
    }
    else {
        const message = "Thank you i am connecting you ta rep";
        return close('Fulfilled', message, session_attributes);
    }
}

async function dispatch(intent_request) {
    console.log("====intent_request======", JSON.stringify(intent_request));

    const intent_name = intent_request['currentIntent']['name'];
    console.log('Intent {} is invoked.', intent_name);
    //console.log("From Enum" + constants.botIntents.int_initiate);

    if (intent_name == 'PCU_SFPP_CB_Initiate') {
        return await initialResponse(intent_request);
    } else if (intent_name == 'PCU_SFPP_Billing_Options') {
        return await customerData(intent_request);
    } else if (intent_name == 'PCU_SFPP_Billing_AllOtherTopics') {
        return await customerData(intent_request);
    } else if (intent_name == 'PCU_SFPP_Billing_CombineAccount') {
        return await customerData(intent_request);
    } else if (intent_name == 'PCU_SFPP_Billing_ChangeAmount') {
        return await customerData(intent_request);
    } else if (intent_name == 'PCU_SFPP_Billing_BillThrough') {
        return await customerData(intent_request);
    } else if (intent_name == 'PCU_SFPP_Billing_Suspending_Draft') {
        return await customerData(intent_request);
    } else if (intent_name == 'PCU_SFPP_Billing_Reinstate_Draft') {
        return await customerData(intent_request);
    } else if (intent_name == 'PCU_SFPP_Billing_ReviewingHistory') {
        return await customerData(intent_request);
    }
}

exports.handler = async (event) => {
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Error In Lambda Function", err);
        return err;
    }
};
