'use strict';
const AWS = require('aws-sdk');
const fetch = require('node-fetch');
const KMS = new AWS.KMS();
const username = process.env.USERNAME;
const password = process.env.PASSWORD;
const url = process.env.URL
//const agrmntKey = agreementAccessKey
//const url = `http://sfesb-sys.opr.test.statefarm.org/B2E/REST/PolicyRetrieval-Web_v2/services/rest/IRetrievePolicyService/policies?agreementAccessKey=${agrmntKey}&asofDate=2020-05-19&filters=POLICY,RISK,PHYOBJCT&dataFormat=AMM&sourceSystemCode=1`;
async function decryption(encrypted) {
    try {
        let decryptData = await KMS.decrypt({ CiphertextBlob: Buffer.from(encrypted, 'base64') }).promise();
        return decryptData.Plaintext.toString('ascii');

    } catch (err) {
        console.error("Exception while decryption", err);
        return err;
    }
}
async function getDVLData(agrmntKey, sourceSystemCode) {
    try {
        const decryptUsername = await decryption(username);
        const decryptPassword = await decryption(password);
        const authencrypt = Buffer.from(decryptUsername + ":" + decryptPassword).toString("base64");
        console.log("==========encryptedAuth==============", authencrypt);
        const url = `${url}agreementAccessKey=${agrmntKey}&asofDate=${new Date()}&filters=POLICY,RISK,PHYOBJCT&dataFormat=AMM&sourceSystemCode=${sourceSystemCode}`;
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json', 'esb_restPath': 'B2E/REST/PolicyRetrieval-Web_v2/TEST5',
                'Authorization': `Basic ${authencrypt}`
            },
        });
        console.log("Retreived data", response);
        const parseData = await response.text();
        console.log("parsed data", parseData);
        const finalData = JSON.parse(parseData);
        console.log("final data", JSON.stringify(finalData));
        return finalData;
    } catch (err) {
        console.error("Error while retrieving data", err);
        return err;
    }
}


exports.handler = async (event) => {
    try {
        const agrmntKey = event["agreementAccessKey"];
        const srcd = event["sourceSystemCode"]
        const data = await getDVLData(agrmntKey, srcd);
        return data;

    } catch (err) {
        console.log("Err in Lambda function", err);
        return err;
    }
};
