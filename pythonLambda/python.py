"""
Submission instructions provided for business auto.
"""
import os
import logging
import boto3
import json

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


""" --- DynamoDb connection set up. --- """
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('sf-conncen-rsch-phka-pcassistantbot')

def get_response(intent_name):
    global table
    intentResponse = {}
    
    # Get the FAQ respnose for given intent_name.
    try:
        response = table.get_item(
            Key={
                'IntentName': intent_name
            }
        )
    except ClientError as e:
        logger.error('Error occured while trying to retrieve response for intent : '.format(intent_name))
        logger.error(e.response['Error']['Message'])
    else:
        intentResponse["Response"] = response['Item']['FAQResponse']
        intentResponse["RelatedQuestions"] = response['Item']['RelatedQuestions']
        logger.info("The related questions are as follows:  {}".format(intentResponse["RelatedQuestions"]))
    logger.debug("The intent resopnse is : {}".format(intentResponse))
    # Finally return the composed intent response to delegate.
    return intentResponse



""" --- Helpers to build responses which match the structure of the necessary dialog actions --- """


def get_slots(intent_request):
    return intent_request['currentIntent']['slots']

def delegate(fulfillment_state, message, related_intents={}):
    dialogResponse = {
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': {
                'contentType': 'CustomPayload',
                'content': message
            }
        }
    }
    # Check if we are given a list of related questions for this intent.
    if(len(related_intents) > 0):
        # Generate tehthe response card to be added to dialogResponse.
        responseCard = {
            'version': 1,
            'contentType': 'application/vnd.amazonaws.card.generic',
            'genericAttachments': []
        }
        questions = []
        # Create buttons for each of the realted intent question.
        for question in related_intents:
            question_button = {
                'text':question,
                'value':question
            }
            questions.append(question_button)
        logger.info("Question buttons are : {}".format(questions))
        # Create the generic attachment to add to the lex response.
        attachment = {
            'title':'You can also ask..',
            'buttons':questions
        }
        logger.info("The generic attachment is : {}".format(attachment))
        # Attach the attachments to the response card.
        responseCard['genericAttachments'].append(attachment)
        # Attach response card to dialogResponse.
        dialogResponse['dialogAction']['responseCard'] = responseCard
        logger.info("The dialogResponse will be formatted as : {}".format(dialogResponse))
    
    return dialogResponse

""" --- Validation Helper Functions --- """


def build_validation_result(is_valid, violated_slot, message_content):
    if message_content is None:
        return {
            "isValid": is_valid,
            "violatedSlot": violated_slot,
        }

    return {
        'isValid': is_valid,
        'violatedSlot': violated_slot,
        'message': {'contentType': 'CustomPayload', 'content': message_content}
    }

def validate_policy_type(policy_type):
    policy_types = ['commercial auto', 'auto', 'business auto', 'auto application', 'auto app']
    if policy_type is not None and policy_type.lower() not in policy_types:
        return build_validation_result(False,
                                       'policyType',
                                       'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.')
    return build_validation_result(True, None, None)

def validate_vehicle_type(vehicle_type):
    return build_validation_result(True, None, None)

def validate_document_type(vehicle_type):
    return build_validation_result(True, None, None)

def validate_line_coverage(policy_type, coverage_type):
    policy_types = ['commercial auto', 'auto', 'business auto', 'auto application', 'auto app']
    if policy_type is not None and policy_type.lower() not in policy_types:
        return build_validation_result(False,
                                       'policyType',
                                       'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.')
    return build_validation_result(True, None, None)

def validate_vehicle_coverage(coverage_type):
    return build_validation_result(True, None, None)

""" --- Functions that control the bot's behavior --- """


def submission_instruction(intent_request):
    """
    Performs validations on policy type slot and then returns new submission instructions as needed.
    """

    policy_type = get_slots(intent_request)["policyType"]

    # Perform basic validation on the supplied input slots.
    validation_result = validate_policy_type(policy_type)
    if not validation_result['isValid']:
        errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage)
    else:
        response = get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"], response["RelatedQuestions"])

def add_vehicle(intent_request):
    """
    Provides instructions on adding a new vehicle to an auto policy.
    """

    vehicle_type = get_slots(intent_request)["vehicleType"]

    # Perform basic validation on the supplied input slots.
    validation_result = validate_vehicle_type(vehicle_type)
    if not validation_result['isValid']:
        errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        return delegate('Fulfilled', errorMessage)
    else:
        response = get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('vehicle_type', vehicle_type), response["RelatedQuestions"])

def generate_document(intent_request):
    """
    Provides instructions on how to generate a new document.
    """

    document_type = get_slots(intent_request)["documentType"]

    # Perform basic validation on the supplied input slots.
    validation_result = validate_document_type(document_type)
    if not validation_result['isValid']:
        errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        return delegate('Fulfilled', errorMessage)
    else:
        response = get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('document_type', document_type), response["RelatedQuestions"])

def line_level_coverage(intent_request):
    """
    Provides instructions on updating line level coverages.
    """

    coverage_type = get_slots(intent_request)["coverageType"]
    policy_type = get_slots(intent_request)["policyType"]
    state_coverages = ['uninsured motor vehicle', 'underinsured motor vehicle', 'state info']
    screen_name = 'Commercial Auto Line'
    if(any(coverage_type.lower() in state_cov for state_cov in state_coverages)) :
        screen_name = 'State Info'

    # Perform basic validation on the supplied input slots.
    validation_result = validate_line_coverage(policy_type, coverage_type)
    if not validation_result['isValid']:
        errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        if(validation_result['violatedSlot'] == 'policyType'):
            errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage)
    else:
        response = get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('coverage_type', coverage_type).replace('screen_name', screen_name), response["RelatedQuestions"])


def vehicle_level_coverage(intent_request):
    """
    Provides instructions on updating vehicle level coverages.
    """

    coverage_type = get_slots(intent_request)["vehicleCoverage"]
    # Perform basic validation on the supplied input slots.
    validation_result = validate_vehicle_coverage(coverage_type)
    if not validation_result['isValid']:
        errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        if(validation_result['violatedSlot'] == 'vehicleCoverage'):
            errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage)
    else:
        response = get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('coverage_type', coverage_type), response["RelatedQuestions"])

""" --- Dispatch Intents --- """

def dispatch(intent_request):
    """
    Called when the user specifies an intent for this bot.
    """

    intent_name = intent_request['currentIntent']['name']
    logger.debug('Intent {} is invoked.'.format(intent_name))
    
    if intent_name == 'submission_instruction':
        return submission_instruction(intent_request)
    elif intent_name == 'add_vehicle':
        return add_vehicle(intent_request)
    elif intent_name == 'generate_document':
        return generate_document(intent_request)
    elif intent_name == 'line_level_coverage':
        return line_level_coverage(intent_request)
    elif intent_name == 'vehicle_level_coverage':
        return vehicle_level_coverage(intent_request)
    else:
        response = get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"], response["RelatedQuestions"])

    raise Exception('Intent with name ' + intent_request['currentIntent']['name'] + ' is not supported at this time.')

""" --- Main handler --- """


def lambda_handler(event, context):
    """
    Route the incoming request based on intent.
    The JSON body of the request is provided in the event slot.
    """
    logger.debug('event.bot.name={}'.format(event['bot']['name']))

    return dispatch(event)
