'use strict';

const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB();
const tableName = process.env.DYNAMODB_TABLE
process.env.AWS_NODEJS_CONNECTION_REUSE_ENABLED = 1;

const close = (fulfillment_state, message, session_attributes = {}) => {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': {
                'contentType': 'PlainText',
                'content': message
            }
        }
    };
    console.log("===========close dialoge response============", dialogResponse);
    return dialogResponse;
};

const elicitSlot = (sessionAttributes, intentName, slots, slotToElicit, message) => {
    const dialogResponse = {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,
        },
    };
    console.log("===========elicitSlot dialoge response============", dialogResponse);
    return dialogResponse;
};

async function confimIntent(session_attributes, intentName, message, slots) {
    const dialogResponse = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            slots,
            'type': 'ConfirmIntent',
            'intentName': intentName,
            'message': {
                'contentType': 'CustomPayload',
                'content': message
            },
            'responseCard': {
                'version': 1,
                'contentType': 'application/vnd.amazonaws.card.generic',
                'genericAttachments': [{
                    'buttons': [
                        { text: 'Yes', value: 'Yes' },
                        { text: 'No', value: 'No' },
                    ]
                }]
            }
        }
    };
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}


async function readDynamo(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const intentName = intent_request['currentIntent']['name'];
    const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];
    const slots = intent_request['currentIntent']['slots'];

    if (confirmationStatus == 'None') {
        console.log("confirmationStatus none called");

        const params = {
            Key: {
                "phoneNumber": {
                    S: slots.phoneNumber
                },
                "dateOfBirth": {
                    S: slots.dateOfBirth
                }
            },
            TableName: tableName
        };
        const readingDynamo = await dynamodb.getItem(params).promise();
        console.log("readingDynamo", readingDynamo);
        const sessionKeys = Object.keys(readingDynamo);
        console.log("sessionKeys", sessionKeys);
        const sessionKeysLength = sessionKeys.length;
        console.log("sessionKeysLength", sessionKeysLength);
        if (sessionKeysLength > 0) {
            const message = 'Auto ID Card sucessfully emailed, Do you need Additional assistance?';
            return confimIntent(session_attributes, intentName, message, slots);
        }
        else {
            const message = 'Auto ID card was not sucessfully emailed, routing you to a rep to assist further';
            return close('Fulfilled', message);
        }
    } else if (confirmationStatus == 'Confirmed') {
        console.log("confirmationStatus Confirmed called");
        const message = `Thank you, I am connecting you to a representative`;
        return close('Fulfilled', message, session_attributes);
    } else if (confirmationStatus == 'Denied') {
        console.log("confirmationStatus Denied called");
        const message = 'Glad to help';
        return close('Fulfilled', message, session_attributes);
    }
}

async function customerData(intent_request) {
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    const slots = intent_request['currentIntent']['slots'];
    const phonenumber = intent_request['currentIntent']['slots']['phoneNumber'];
    const dateofbirth = intent_request['currentIntent']['slots']['dateOfBirth'];
    console.log("outside  part", intent_request['currentIntent']['slots']);
    //  const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];
    if (phonenumber != null && dateofbirth != null) {
        console.log("inside the phonenumber and dob is not null part");
        const filteredNumber = phonenumber.replace(/[^\d]/g, '');
        console.log("filteredNumber", filteredNumber);
        if (filteredNumber.length == 10) {
            console.log("=======filteredNumber length is 10==========", filteredNumber);
            intent_request['currentIntent']['slots']['phoneNumber'] = filteredNumber;
        } else {
            console.log("=======filteredNumber length is > 10==========", filteredNumber);
            const finalNumber = filteredNumber.substr(1);
            intent_request['currentIntent']['slots']['phoneNumber'] = finalNumber;
        }
        console.log("phonenumber", intent_request['currentIntent']['slots']['phoneNumber']);

        return await readDynamo(intent_request);

    } else if (phonenumber != null) {
        console.log("inside the phonenumber is not null part", intent_request['currentIntent']['slots']);
        return elicitSlot(session_attributes, intentName, slots, 'dateOfBirth');
    } else if (phonenumber == null) {
        console.log("inside the phonenumber null part", intent_request['currentIntent']['slots']);
        return elicitSlot(session_attributes, intentName, slots, 'phoneNumber');
    }
}

async function dispatch(intent_request) {
    console.log("====intent_request======", JSON.stringify(intent_request));

    const intent_name = intent_request['currentIntent']['name'];
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'AutoID_customer_data') {
        return await customerData(intent_request);
    }


}

exports.handler = async (event) => {
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
};
