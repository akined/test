'use strict';
const AWS = require('aws-sdk');
// ---------------------------------------------------- DynamoDb connection set up. ------------------------------------------ //
const dynamodb = new AWS.DynamoDB({});
const descriptors = ['L', 'M', 'N', 'S', 'B', 'SS', 'NULL', 'NS', 'BS'];
const flattenDynamoDbResponse = (o) => {

    // flattens single property objects that have descriptors  
    for (let d of descriptors) {
        if (o.hasOwnProperty(d)) {
            return o[d];
        }
    }

    Object.keys(o).forEach((k) => {

        for (let d of descriptors) {
            if (o[k].hasOwnProperty(d)) {
                o[k] = o[k][d];
            }
        }
        if (Array.isArray(o[k])) {
            o[k] = o[k].map(e => flattenDynamoDbResponse(e));
        } else if (typeof o[k] === 'object') {
            o[k] = flattenDynamoDbResponse(o[k]);
        }
    });

    return o;
};

async function get_response(params, intent_name) {
    try {
        console.log("========= params ========", JSON.stringify(params));
        const dynamodbResponse = await dynamodb.getItem(params).promise();
        console.log("========dyanmoresponse========", JSON.stringify(dynamodbResponse));

        const intentResponse = {
            Response: dynamodbResponse,
        };
        console.log("The intent resopnse is :", intentResponse);

        return intentResponse;
    } catch (err) {
        console.log("Error occured while trying to retrieve response for intent: ", intent_name);
        console.log(err);
        return err.response['Error']['Message'];
    }
}

async function batch_get_response(params, intent_name) {
    try {
        console.log("========= params =====batchGetItem===", JSON.stringify(params));
        const dynamodbResponse = await dynamodb.batchGetItem(params).promise();
        console.log("========dyanmoresponse========", JSON.stringify(dynamodbResponse));

        console.log("The dynamodbResponse resopnse is batchGetItem:", dynamodbResponse);

        return dynamodbResponse;
    } catch (err) {
        console.log("Error occured while trying to retrieve response for intent: ", intent_name);
        console.log(err);
        return err.response['Error']['Message'];
    }
}

/*async function scanDynamoResponse(params) {
    try {
        console.log("params ===========", JSON.stringify(params));
        const resopnse = await dynamodb.scan(params).promise();
        console.log("resopnse======", resopnse);
        return resopnse;
    } catch (err) {
        console.log("Error occured: ", params);
        console.log(err);
        return err;
    }
};

async function callBatchWriteItemDynamo(params) {
    try {
        console.log("params for inserting data ===========", JSON.stringify(params));
        // Call DynamoDB to add the item to the table
        const dynamoDBResponse = await dynamodb.batchWriteItem(params).promise();
        console.log("dynamoDBResponse", dynamoDBResponse);
        return dynamoDBResponse;
    } catch (err) {
        console.log("Error occured while trying to save the response for intent: ", params);
        console.log(err);
        return err;
    }
};*/
// ---------------- Helpers to build responses which match the structure of the necessary dialog actions ----------------------- //

const close = (fulfillment_state, message, session_attributes = {}) => {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': {
                'contentType': 'PlainText',
                'content': message
            }
        }
    };
    console.log("===========close dialoge response============", dialogResponse);
    return dialogResponse;
};

const confirmIntent = (message, intent_request = {}, slotDetails, session_attributes = {}) => {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intent_request['currentIntent']['name'],
            'message': message,
            slots: slotDetails,
        }
    };
    console.log("================confirmIntent dialogResponse===========", message);
    return dialogResponse;
};

const elicitSlot = (sessionAttributes, intentName, slots, slotToElicit, message) => {
    const dialogResponse = {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,
        },
    };
    console.log("===========elicitSlot dialoge response============", dialogResponse);
    return dialogResponse;
};

const elicitIntent = (sessionAttributes, intentName, message) => {
    const dialogResponse = {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitIntent',
            message
        }
    };
    console.log("=====elicitIntent dialog Response=====", dialogResponse);
    return dialogResponse;

};

async function buttonsForMakeModelYear(agreementData, pmrData, clientData) {
    const dialogResponse = {
        'sessionAttributes': {
            agreement: JSON.stringify(agreementData),
            pmr: JSON.stringify(pmrData),
            client: JSON.stringify(clientData),
        },
        'dialogAction': {
            'type': 'ElicitIntent',
            //'fulfillmentState': 'Fulfilled',
            'message': {
                'contentType': 'CustomPayload',
                'content': "Please Click on the make model and year of the vehicle you want to place the order for"
            },

        }
    };
    const responseCard = {
        'version': 1,
        'contentType': 'application/vnd.amazonaws.card.generic',
        'genericAttachments': []
    };
    console.log("========agreementData==========butons===", agreementData);
    const questions = agreementData.Response.Item.map((data) => ({
        text: `${data.Make}, ${data.Model}, ${data.Year}`,
        value: `Make-Model-Year,${data.Make},${data.Model},${data.Year}`,
    }));

    const totalFourQuestions = questions.slice(0, 4);
    console.log("totalFourQuestions buttons are ", totalFourQuestions);

    totalFourQuestions.push(
        {
            text: 'Not listed',
            value: 'NotListed'
        });

    console.log("Question buttons are ", totalFourQuestions);
    // Create the generic attachment to add to the lex response.
    const attachment = {
        //'title': 'You can also ask..',
        'buttons': totalFourQuestions
    };
    console.log("The generic attachment is : ", attachment);

    responseCard['genericAttachments'].push(attachment);
    dialogResponse['dialogAction']['responseCard'] = responseCard;
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}

async function elicitIntentResponseWithButtons(agreementData, pmrData, clientData) {
    const dialogResponse = {
        'sessionAttributes': {
            agreement: JSON.stringify(agreementData),
            pmr: JSON.stringify(pmrData),
            client: JSON.stringify(clientData),
        },
        'dialogAction': {
            'type': 'ElicitIntent',
            //'fulfillmentState': 'Fulfilled',
            'message': {
                'contentType': 'CustomPayload',
                'content': "Please Select a Valid reason for Reordering Beacon"
            },

        }
    };
    const responseCard = {
        'version': 1,
        'contentType': 'application/vnd.amazonaws.card.generic',
        'genericAttachments': []
    };
    const questions = [
        { text: 'Sent to wrong address', value: 'Sent to wrong address' },
        { text: 'No red light when button held down', value: 'No red light when button held down' },
        { text: 'Left in prior vehicle/Re-enrollment', value: 'Left in prior vehicle/Re-enrollment' },
        { text: 'Misplaced / Stolen', value: 'Misplaced / Stolen' },
        { text: 'None of the above', value: 'None of the above' },
    ];
    console.log("Question buttons are ", questions);
    // Create the generic attachment to add to the lex response.
    const attachment = {
        //'title': 'You can also ask..',
        'buttons': questions
    };
    console.log("The generic attachment is : ", attachment);

    responseCard['genericAttachments'].push(attachment);
    dialogResponse['dialogAction']['responseCard'] = responseCard;
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}
// ----------------------------------------- Functions that control the bot's behavior --------------------------------------- //
async function getAlias(intent_request) {
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    const slots = intent_request['currentIntent']['slots'];
    const dynamoTable = 'sf-conncen-rsch-usw1-pcpodd-vaafeh-actorinquiry';
    const aliasValue = intent_request['currentIntent']['slots']['alias'];
    const params = {
        TableName: dynamoTable,
        Key: {
            'Alias': {
                S: aliasValue.toUpperCase()
            }
        },
    };
    const response = await get_response(params, aliasValue);
    if (Object.keys(response['Response']).length > 0) {
        return elicitSlot(session_attributes, intentName, slots, 'phonenumber');
    } else {
        const message = "Connecting To Rep";
        return close('Fulfilled', message, session_attributes);
    }
}

async function getDiscount(intent_request, agreementData, clientData) {
    console.log("========agreementData======discount called=", JSON.stringify(agreementData));
    const session_attributes = intent_request["sessionAttributes"];
    const agreementKey = agreementData['Response']['Item']['agreementAccessKey'];
    const dynamoTable = 'sf-conncen-rsch-usw1-pcpodd-vaafeh-dvl1';
    const agreementKeysForLookup = agreementKey.map((data) => ({
        agreementAccessKey: { S: data }
    }));
    const params = {
        RequestItems: {
            [dynamoTable]: {
                Keys: agreementKeysForLookup
            }
        }

    };
    const aliasValue = intent_request['currentIntent']['slots']['alias'];

    //const response_dynamo = await scanDynamoResponse(params);
    const response_dynamo = await batch_get_response(params, aliasValue);

    console.log("=====response_dynamo=========", JSON.stringify(response_dynamo));
    if (response_dynamo && response_dynamo.Responses && response_dynamo.Responses[dynamoTable].length > 0) {
        const dynamodbFlattenedData = response_dynamo.Responses[dynamoTable].map((item) => flattenDynamoDbResponse(item));
        console.log("=====dynamodbFlattenedData=========", JSON.stringify(dynamodbFlattenedData));

        const filterData = dynamodbFlattenedData.filter((data) => data.pricingRuleSetTypeCode == 1);
        console.log("=====filterData=========", filterData);
        if (filterData.length > 0) {
            const finalResponse = {
                Response: {
                    Item: filterData
                }
            };
            return await buttonsForMakeModelYear(finalResponse, agreementData, clientData);
        }
        else {
            const message = "Connecting To Rep";
            return close('Fulfilled', message, session_attributes);
        }
    } else {
        const message = "Connecting To Rep";
        return close('Fulfilled', message, session_attributes);
    }
}

async function getAgreementData(intent_request, clientData) {
    console.log("========clientData======pmr called=", JSON.stringify(clientData));
    console.log("========intent_request======getAgreementData called=", JSON.stringify(intent_request['currentIntent']['slots']));
    const session_attributes = intent_request["sessionAttributes"];
    const clientId = clientData['Response']['Item']['ClientID'];
    const dynamoTable = 'sf-conncen-rsch-usw1-pcpodd-vaafeh-agreementindex';
    const params = {
        TableName: dynamoTable,
        Key: {
            'Client ID': {
                S: clientId
            },
        }
    };
    const aliasValue = intent_request['currentIntent']['slots']['alias'];
    const response = await get_response(params, aliasValue);
    if (Object.keys(response['Response']).length > 0) {
        const agreementData = flattenDynamoDbResponse(response);
        return await getDiscount(intent_request, agreementData, clientData);
    } else {
        const message = `Connecting To Representative`;
        return close('Fulfilled', message, session_attributes);
    }
}

async function getClientId(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const phonenumber = intent_request['currentIntent']['slots']['phonenumber'];
    const dateofbirth = intent_request['currentIntent']['slots']['dateofbirth'];
    const dynamoTable = 'sf-conncen-rsch-usw1-pcpodd-vaafeh-customerdata';
    const params = {
        TableName: dynamoTable,
        Key: {
            'PhoneNumber': {
                S: phonenumber
            },
            'DateOfBirth': {
                S: dateofbirth
            }
        },
    };
    const aliasValue = intent_request['currentIntent']['slots']['alias'];
    const response = await get_response(params, aliasValue);
    if (Object.keys(response['Response']).length > 0) {
        const finalResponse = flattenDynamoDbResponse(response);
        return await getAgreementData(intent_request, finalResponse);
    } else {
        const message = `Connecting To Representative\n Phone Number: ${intent_request['currentIntent']['slots']['phonenumber']}\n
        Date of Birth: ${intent_request['currentIntent']['slots']['dateofbirth']}`;
        return close('Fulfilled', message, session_attributes);
    }
}


async function customerData(intent_request) {
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    const slots = intent_request['currentIntent']['slots'];
    const phonenumber = intent_request['currentIntent']['slots']['phonenumber'];
    const dateofbirth = intent_request['currentIntent']['slots']['dateofbirth'];
    if (phonenumber != null && dateofbirth != null) {
        // return elicitSlot(session_attributes, intentName, slots, 'year');
        return await getClientId(intent_request);
    } else if (phonenumber != null) {
        return elicitSlot(session_attributes, intentName, slots, 'dateofbirth');
    } else {
        return getAlias(intent_request);
    }
}

async function placeOrder(dynamodbTable, intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const parsedClientData = JSON.parse(session_attributes['client']);
    const parsedAgreementData = JSON.parse(session_attributes['agreement']);
    const parsedPmrData = JSON.parse(session_attributes['pmr']);

    console.log("======parsedClientData============", parsedClientData);
    console.log("======parsedAgreementData============", parsedAgreementData);
   // const clientId = parsedClientData['Response']['Item']['ClientID'];
    const year = parsedAgreementData['Response']['Item']['Year'];
    const make = parsedAgreementData['Response']['Item']['Make'];
    const model = parsedAgreementData['Response']['Item']['Model'];
    const vinNumber = parsedAgreementData['Response']['Item']['VIN'];
    const customerName = parsedClientData['Response']['Item']['CustomerName'];
    const customerPhoneNumber = parsedClientData['Response']['Item']['PhoneNumber'];
    const mailingAddr = parsedClientData['Response']['Item']['CustomerMailingAddress'];
    const policyNumber = parsedPmrData['Response']['Item']['PolicyNumber'];

    const message = `Thank you i am sending you to a rep to order a beacon for\n
                    ClientName: ${customerName},\n
                    PhoneNumber: ${customerPhoneNumber},\n
                    Address:${mailingAddr},\n
                    PolicyNumber: ${policyNumber},\n
                    VIN: ${vinNumber},\n
                    Year, Make, Model: ${year}, ${make}, ${model}`;
    return close('Fulfilled', message, session_attributes);
}

async function wrongAddressData(intent_request) {
    console.log("==========session_attributes=======", JSON.stringify(intent_request["sessionAttributes"]));
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    const slots = intent_request['currentIntent']['slots'];
    const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];

    const parsedClientData = JSON.parse(session_attributes['client']);
    const mailingAddr = parsedClientData['Response']['Item']['CustomerMailingAddress'];

    if (confirmationStatus == 'None') {
        const dialogResponse = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ConfirmIntent',
                'intentName': intentName,
                'message': {
                    'contentType': 'CustomPayload',
                    'content': `Is this the right mailing address ${mailingAddr}. Please Select The Following`
                },
                slots,
                'responseCard': {
                    'version': 1,
                    'contentType': 'application/vnd.amazonaws.card.generic',
                    'genericAttachments': [{
                        'buttons': [
                            { text: 'Yes', value: 'Yes' },
                            { text: 'No', value: 'No' },
                        ]
                    }]
                }
            }
        };
        console.log("The dialogResponse will be formatted as : ", dialogResponse);
        return dialogResponse;
    } else if (confirmationStatus == 'Denied') {
        const message = `Connecting To Representative`
        ;
        return close('Fulfilled', message, session_attributes);
    }
    else if (confirmationStatus == 'Confirmed') {
        const dynamodbTable = 'sf-conncen-rsch-usw1-vaafeh-pcpodd-BDM';
        return await placeOrder(dynamodbTable, intent_request);
    }
}
async function notListed(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const parsedClientData = JSON.parse(session_attributes['client']);
    const parsedAgreementData = JSON.parse(session_attributes['agreement']);
    const parsedPmrData = JSON.parse(session_attributes['pmr']);

    console.log("======parsedClientData============", parsedClientData);
    console.log("======parsedAgreementData============", parsedAgreementData);
    const customerName = parsedClientData['Response']['Item']['CustomerName'];
    const customerPhoneNumber = parsedClientData['Response']['Item']['PhoneNumber'];
    const mailingAddr = parsedClientData['Response']['Item']['CustomerMailingAddress'];
    const policyNumber = parsedPmrData['Response']['Item']['PolicyNumber'];

    const message = `Thank you i am sending you to a rep to order a beacon for\n
                    ClientName: ${customerName},\n
                    Address: ${mailingAddr},\n
                    PhoneNumber: ${customerPhoneNumber},\n
                    PolicyNumber: ${policyNumber},\n`;
                   
    return close('Fulfilled', message, session_attributes);
}

async function noRedLight(intent_request) {
    const dynamodbTable = 'sf-conncen-rsch-usw1-vaafeh-pcpodd-BDM';
    return await placeOrder(dynamodbTable, intent_request);

}

async function noneOfAbove(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const parsedClientData = JSON.parse(session_attributes['client']);
    const parsedAgreementData = JSON.parse(session_attributes['agreement']);
    const parsedPmrData = JSON.parse(session_attributes['pmr']);

    console.log("======parsedClientData============", parsedClientData);
    console.log("======parsedAgreementData============", parsedAgreementData);
    const year = parsedAgreementData['Response']['Item']['Year'];
    const make = parsedAgreementData['Response']['Item']['Make'];
    const model = parsedAgreementData['Response']['Item']['Model'];
    const vinNumber = parsedAgreementData['Response']['Item']['VIN'];
    const customerName = parsedClientData['Response']['Item']['CustomerName'];
    const customerPhoneNumber = parsedClientData['Response']['Item']['PhoneNumber'];
    const mailingAddr = parsedClientData['Response']['Item']['CustomerMailingAddress'];
    const policyNumber = parsedPmrData['Response']['Item']['PolicyNumber'];
    
    const message = `Thank you i am sending you to a rep to order a beacon for\n
                    ClientName: ${customerName},\n
                    Address: ${mailingAddr},\n
                    PhoneNumber: ${customerPhoneNumber},\n
                    PolicyNumber: ${policyNumber},\n
                    VIN: ${vinNumber},\n
                    Year, Make, Model: ${year}, ${make}, ${model}`;
    return close('Fulfilled', message, session_attributes);
}

async function yearMakeModel(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const parsedClientData = JSON.parse(session_attributes['client']);
    const parsedAgreementData = JSON.parse(session_attributes['agreement']);
    const parsedPmrData = JSON.parse(session_attributes['pmr']);

    console.log("======parsedAgreementData============", JSON.stringify(parsedAgreementData));
    const input = intent_request['inputTranscript'];
    console.log("======input============", input);
    const splittedValues = input.split(',');
    const make = splittedValues[1];
    const model = splittedValues[2];
    const year = splittedValues[3];
    console.log("======make============", make);
    console.log("======model============", model);
    console.log("======year============", year);
    const filterAgreementData = parsedAgreementData.Response.Item.find((data) => {
        console.log("==============data=====", data);
        return data.Make == make
            && data.Model == model && data.Year == year;
    });
    console.log("===========filtererdata", filterAgreementData);

    if (filterAgreementData != undefined) {
        const finalResponse = {
            Response: {
                Item: filterAgreementData
            }
        };
        return await elicitIntentResponseWithButtons(finalResponse, parsedPmrData, parsedClientData);
    } else {
        const message = "Connecting To Rep";
        return close('Fulfilled', message, session_attributes);
    }
}

// ------------------------------------------ Dispatch Intents ---------------------------------------------- //

async function dispatch(intent_request) {
    console.log("====intent_request======", JSON.stringify(intent_request));

    const intent_name = intent_request['currentIntent']['name'];
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'CustomerData') {
        return await customerData(intent_request);
    } else if (intent_name == 'WrongAddress') {
        return await wrongAddressData(intent_request);
    } else if (intent_name == 'NoRedLight') {
        return await noRedLight(intent_request);
    } else if (intent_name == 'NoneofAbove') {
        return await noneOfAbove(intent_request);
    } else if (intent_name == 'YearMakeModel') {
        return await yearMakeModel(intent_request);
    } else if (intent_name == 'NotListed') {
        return await notListed(intent_request);
    }    
}

// ---------------------------------------------------- Main handler ------------------------------------------------- //

exports.handler = async function (event, context) {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
};

