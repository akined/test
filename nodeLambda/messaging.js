'use strict';
const livePersonClientPost = require('./lib/livePersonClient');
const buildMsgHistURL = require('./lib//msgHistURLBuilder');
const buildRequestBody = require('./lib/msgHistRequestBuilder');


//TODO: Vault integration? 
//Notes from meeting: Credentials probably retrieved from separate Lambda function (Dependency with Architecture)
//TODO: Determine VPC/Proxy usage
//Notes from meeting: Need to confirm with security if VPC/proxy needed since Lambda is not making any internal calls
//If proxy needed can configure through Axios client with proxy creds retrieved folrom Vault.
async function getConversation(event) {

    let res, url, body, account, convoId;

    try {

        convoId = event['conversationId'];
        account = event['account'];
        url = await buildMsgHistURL(account);
        body = await buildRequestBody(convoId);

        res = {};

        res = await livePersonClientPost(account, url, body);


    } catch (err) {
        err.message = `Messaging History Service Error - URL:${url} BODY:${JSON.stringify(body)} with Error: ${err.message}`;
        console.error(err);
        throw err;
    }

    return {
        statusCode: 200,
        body: res
    };

}

exports.handler = async (event) => {

    // const response = await getConversation(event);

    const response = {
        "body": {
            "_metadata": {
                "count": 1,
                "self": {
                    "rel": "self",
                    "href": "http://va.msghist.liveperson.net/messaging_history/api/account/8917914/conversations/conversation/search?limit=50&offset=0&v=2"
                },
                "shardsStatusResult": {
                    "partialResult": false
                }
            },
            "conversationHistoryRecords": [
                {
                    "info": {
                        "startTime": "2020-10-06 16:34:09.070+0000",
                        "startTimeL": 1602002049070,
                        "endTime": "undefined",
                        "endTimeL": -1,
                        "duration": 108174,
                        "conversationId": "75bf8441-1979-44e7-ae3c-237b886ec35a",
                        "brandId": "8917914",
                        "latestAgentId": "-1",
                        "latestAgentNickname": "NA",
                        "latestAgentFullName": "NA",
                        "latestAgentLoginName": "NA",
                        "latestSkillId": 2469210630,
                        "latestSkillName": "PLCC Virtual Assistant",
                        "source": "SHARK",
                        "mcs": 0,
                        "alertedMCS": 0,
                        "status": "OPEN",
                        "fullDialogStatus": "OPEN",
                        "firstConversation": false,
                        "device": "DESKTOP",
                        "browser": "CHROME",
                        "browserVersion": "85.0.4183.121",
                        "operatingSystem": "WINDOWS",
                        "operatingSystemVersion": "10.0",
                        "latestAgentGroupId": 0,
                        "latestAgentGroupName": "NA",
                        "latestQueueState": "IN_QUEUE",
                        "isPartial": false,
                        "visitorId": "U3NTJkNjQyMWM5ZGJmYzcw",
                        "sessionId": "ATRGyQv1Rme_eHufYXZAyw",
                        "interactionContextId": "17",
                        "features": [
                            "PHOTO_SHARING",
                            "CO_BROWSE",
                            "QUICK_REPLIES",
                            "MARKDOWN_HYPERLINKS",
                            "AUTO_MESSAGES",
                            "MULTI_DIALOG",
                            "FILE_SHARING",
                            "RICH_CONTENT"
                        ],
                        "language": "en-US",
                        "integration": "WEB_SDK",
                        "integrationVersion": "3.0.39",
                        "appId": "webAsync",
                        "ipAddress": "206.80.128.24"
                    },
                    "campaign": {
                        "campaignEngagementId": "2471585430",
                        "campaignEngagementName": "PLCC Routing bot",
                        "campaignId": "2471582530",
                        "campaignName": "plccroutingbot Test",
                        "goalId": "55384314",
                        "goalName": "Interact with visitors",
                        "engagementSource": "WEB_SITE",
                        "visitorBehaviorId": "55384414",
                        "visitorBehaviorName": "Any behavior",
                        "visitorProfileId": "55384214",
                        "visitorProfileName": "All visitors",
                        "lobId": 1264216814,
                        "lobName": "Underwriting",
                        "locationId": "2471585330",
                        "locationName": "section:plccroutingbot",
                        "profileSystemDefault": true,
                        "behaviorSystemDefault": true
                    },
                    "messageRecords": [
                        {
                            "type": "TEXT_PLAIN",
                            "messageData": {
                                "msg": {
                                    "text": "App/PT Status"
                                }
                            },
                            "messageId": "ms::dialog:75bf8441-1979-44e7-ae3c-237b886ec35a::msg:0",
                            "audience": "ALL",
                            "seq": 0,
                            "dialogId": "75bf8441-1979-44e7-ae3c-237b886ec35a",
                            "participantId": "5ed2c49778361e1e596b13218799c1a183af2cd175f6a1c01d8ee91a7cabc2c4",
                            "time": "2020-10-06 16:34:09.328+0000",
                            "timeL": 1602002049328,
                            "device": "DESKTOP",
                            "sentBy": "Consumer"
                        }
                    ],
                    "agentParticipants": [
                        {
                            "agentFullName": "SCBot",
                            "agentNickname": "SCBot",
                            "agentLoginName": "SCBot",
                            "agentDeleted": false,
                            "agentId": "1753543230",
                            "agentPid": "15334029-0808-580a-afec-6a6ea0bef116",
                            "userType": "2",
                            "userTypeName": "Bot",
                            "role": "AGENTMANAGER",
                            "agentGroupName": "Main Group",
                            "agentGroupId": -1,
                            "time": "2020-10-06 16:34:09.110+0000",
                            "timeL": 1602002049110,
                            "permission": "MANAGER",
                            "dialogId": "75bf8441-1979-44e7-ae3c-237b886ec35a"
                        },
                        {
                            "agentFullName": "liveperson-controller-bot",
                            "agentNickname": "Autobot",
                            "agentLoginName": "liveperson-controller-bot",
                            "agentDeleted": false,
                            "agentId": "1515405414",
                            "agentPid": "1764939c-e49e-562d-8260-e09616f62635",
                            "userType": "0",
                            "userTypeName": "System",
                            "role": "AGENT",
                            "agentGroupName": "Main Group",
                            "agentGroupId": -1,
                            "time": "2020-10-06 16:34:09.223+0000",
                            "timeL": 1602002049223,
                            "permission": "CONTROLLER",
                            "dialogId": "75bf8441-1979-44e7-ae3c-237b886ec35a"
                        },
                        {
                            "agentFullName": "Nick Reffett",
                            "agentNickname": "Nick Reffett",
                            "agentLoginName": "DRHH",
                            "agentDeleted": false,
                            "agentId": "59271414",
                            "agentPid": "376c7f84-9adc-5777-9063-8233620467b4",
                            "userType": "1",
                            "userTypeName": "Human",
                            "role": "AGENT",
                            "agentGroupName": "Main Group",
                            "agentGroupId": -1,
                            "time": "2020-10-06 16:35:34.664+0000",
                            "timeL": 1602002134664,
                            "permission": "READER",
                            "dialogId": "75bf8441-1979-44e7-ae3c-237b886ec35a"
                        }
                    ],
                    "agentParticipantsActive": [
                        {
                            "agentFullName": "Nick Reffett",
                            "agentNickname": "Nick Reffett",
                            "agentLoginName": "DRHH",
                            "agentDeleted": false,
                            "agentId": "59271414",
                            "agentPid": "376c7f84-9adc-5777-9063-8233620467b4",
                            "userType": "1",
                            "userTypeName": "Human",
                            "role": "AGENT",
                            "agentGroupName": "Main Group",
                            "agentGroupId": -1,
                            "time": "2020-10-06 16:35:34.664+0000",
                            "timeL": 1602002134664,
                            "permission": "READER",
                            "dialogId": "75bf8441-1979-44e7-ae3c-237b886ec35a"
                        },
                        {
                            "agentFullName": "SCBot",
                            "agentNickname": "SCBot",
                            "agentLoginName": "SCBot",
                            "agentDeleted": false,
                            "agentId": "1753543230",
                            "agentPid": "15334029-0808-580a-afec-6a6ea0bef116",
                            "userType": "2",
                            "userTypeName": "Bot",
                            "role": "AGENTMANAGER",
                            "agentGroupName": "Main Group",
                            "agentGroupId": -1,
                            "time": "2020-10-06 16:34:09.110+0000",
                            "timeL": 1602002049110,
                            "permission": "MANAGER",
                            "dialogId": "75bf8441-1979-44e7-ae3c-237b886ec35a"
                        },
                        {
                            "agentFullName": "liveperson-controller-bot",
                            "agentNickname": "Autobot",
                            "agentLoginName": "liveperson-controller-bot",
                            "agentDeleted": false,
                            "agentId": "1515405414",
                            "agentPid": "1764939c-e49e-562d-8260-e09616f62635",
                            "userType": "0",
                            "userTypeName": "System",
                            "role": "AGENT",
                            "agentGroupName": "Main Group",
                            "agentGroupId": -1,
                            "time": "2020-10-06 16:34:09.223+0000",
                            "timeL": 1602002049223,
                            "permission": "CONTROLLER",
                            "dialogId": "75bf8441-1979-44e7-ae3c-237b886ec35a"
                        }
                    ],
                    "consumerParticipants": [
                        {
                            "participantId": "5ed2c49778361e1e596b13218799c1a183af2cd175f6a1c01d8ee91a7cabc2c4",
                            "firstName": "Nick Reffett",
                            "lastName": "undefined",
                            "token": "undefined",
                            "email": "undefined",
                            "phone": "undefined",
                            "avatarURL": "undefined",
                            "time": "2020-08-18 16:11:11.502+0000",
                            "timeL": 1597767071502,
                            "joinTime": "2020-10-06 16:34:09.074+0000",
                            "joinTimeL": 1602002049074,
                            "consumerName": "Nick Reffett",
                            "dialogId": "75bf8441-1979-44e7-ae3c-237b886ec35a"
                        }
                    ],
                    "transfers": [],
                    "interactions": [],
                    "dialogs": [
                        {
                            "dialogId": "75bf8441-1979-44e7-ae3c-237b886ec35a",
                            "status": "OPEN",
                            "dialogType": "MAIN",
                            "dialogChannelType": "MESSAGING",
                            "startTime": "2020-10-06 16:34:09.070+0000",
                            "startTimeL": 1602002049070,
                            "skillId": 2469210630,
                            "skillName": "PLCC Virtual Assistant"
                        }
                    ],
                    "messageScores": [
                        {
                            "messageId": "ms::dialog:75bf8441-1979-44e7-ae3c-237b886ec35a::msg:0",
                            "messageRawScore": 0,
                            "mcs": 0,
                            "time": "2020-10-06 16:34:09.631+0000",
                            "timeL": 1602002049631
                        }
                    ],
                    "messageStatuses": [],
                    "skillChanges": [],
                    "conversationSurveys": [],
                    "unAuthSdes": {
                        "events": [
                            {
                                "customerInfo": {
                                    "serverTimeStamp": 1601310075530,
                                    "originalTimeStamp": 1601299414970,
                                    "customerInfo": {
                                        "customerId": "c0771d06-f9c0-4ce2-b71d-06f9c01ce2f1"
                                    }
                                },
                                "serverTimeStamp": 1601310075530,
                                "sdeType": "CUSTOMER_INFO"
                            },
                            {
                                "customerInfo": {
                                    "serverTimeStamp": 1601999970628,
                                    "originalTimeStamp": 1601999970628,
                                    "customerInfo": {
                                        "customerStatus": "13",
                                        "socialId": "DRHH",
                                        "storeNumber": "13-2687"
                                    }
                                },
                                "serverTimeStamp": 1601999970628,
                                "sdeType": "CUSTOMER_INFO"
                            },
                            {
                                "customerInfo": {
                                    "serverTimeStamp": 1601310075530,
                                    "originalTimeStamp": 1601299414970,
                                    "customerInfo": {
                                        "customerId": "c0771d06-f9c0-4ce2-b71d-06f9c01ce2f1"
                                    }
                                },
                                "serverTimeStamp": 1601310075530,
                                "sdeType": "CUSTOMER_INFO"
                            },
                            {
                                "customerInfo": {
                                    "serverTimeStamp": 1601999970628,
                                    "originalTimeStamp": 1601999970628,
                                    "customerInfo": {
                                        "customerStatus": "13",
                                        "socialId": "DRHH",
                                        "storeNumber": "13-2687"
                                    }
                                },
                                "serverTimeStamp": 1601999970628,
                                "sdeType": "CUSTOMER_INFO"
                            }, {
                                "customerInfo": {
                                    "serverTimeStamp": 1601310075530,
                                    "originalTimeStamp": 1601299414970,
                                    "customerInfo": {
                                        "customerId": "c0771d06-f9c0-4ce2-b71d-06f9c01ce2f1"
                                    }
                                },
                                "serverTimeStamp": 1601310075530,
                                "sdeType": "CUSTOMER_INFO"
                            }, {
                                "customerInfo": {
                                    "serverTimeStamp": 1601310075530,
                                    "originalTimeStamp": 1601299414970,
                                    "customerInfo": {
                                        "customerId": "c0771d06-f9c0-4ce2-b71d-06f9c01ce2f1"
                                    }
                                },
                                "serverTimeStamp": 1601310075530,
                                "sdeType": "CUSTOMER_INFO"
                            }
                        ]
                    },
                    "responseTime": {
                        "latestEffectiveResponseDueTime": 1602002349089,
                        "configuredResponseTime": 300
                    },
                    "monitoring": {
                        "country": "United States",
                        "countryCode": "US",
                        "state": "",
                        "city": "",
                        "isp": "State Farm Mutual Automobile Insurance Company",
                        "org": "State Farm Mutual Automobile Insurance Company",
                        "device": "DESKTOP",
                        "ipAddress": "206.80.128.24",
                        "browser": "Chrome 85.0.4183.121",
                        "operatingSystem": "Windows",
                        "conversationStartPage": "https://static1-env1.test.st8fm.com/en_US/SFChat/Applications/LiveChat/8917914PLCCRoutingBotTest.html",
                        "conversationStartPageTitle": "PLCC Routingbot Sample Page"
                    },
                    "intents": [],
                    "previouslySubmittedAgentSurveys": []
                }
            ]
        }
    };

    console.log("response", JSON.stringify(response));



    if (JSON.stringify(response.body.conversationHistoryRecords[0].unAuthSdes)) {
        const historyrecord = response.body.conversationHistoryRecords[0].unAuthSdes
        console.log("history", historyrecord);
        const filterData = historyrecord.events
            && historyrecord.events.filter(data => data.customerInfo && data.customerInfo.customerInfo &&
                data.customerInfo.customerInfo.customerStatus && (data.customerInfo.customerInfo.customerStatus.length == 2)
                && data.customerInfo.customerInfo.storeNumber && (data.customerInfo.customerInfo.storeNumber.length == 7));
        console.log("======filterData=======", JSON.stringify(filterData));
        const finalData = Array.isArray(filterData) && filterData[0].customerInfo.customerInfo;
        return {
            statusCode: 200,
            message: { customerStaus: finalData.customerStatus, storeNumber: finalData.storeNumber },
        };
    }
    else {
        const message = 'no data found';
        return {
            statusCode: 400,
            message: message
        };
    }





};
