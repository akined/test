'use strict';

const AWS = require('aws-sdk');
const stepFunctions = new AWS.StepFunctions({});
const stepFn = process.env.STEP_FUNCTION;
process.env.AWS_NODEJS_CONNECTION_REUSE_ENABLED = 1;

const close = (fulfillment_state, message, session_attributes = {}) => {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': {
                'contentType': 'PlainText',
                'content': message
            }
        }
    };
    console.log("===========close dialoge response============", dialogResponse);
    return dialogResponse;
};

const elicitSlot = (sessionAttributes, intentName, slots, slotToElicit, message) => {
    const dialogResponse = {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,
        },
    };
    console.log("===========elicitSlot dialoge response============", dialogResponse);
    return dialogResponse;
};

const confirmIntent = (intentName, message, session_attributes) => {
    const dialogResponse = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intentName,
            'message': {
                'contentType': 'CustomPayload',
                'content': message
            },
            'responseCard': {
                'version': 1,
                'contentType': 'application/vnd.amazonaws.card.generic',
                'genericAttachments': [{
                    'buttons': [
                        { text: 'Yes', value: 'Yes' },
                        { text: 'No', value: 'No' },
                    ]
                }]
            }
        }
    };
    return dialogResponse;
}


const ElicitIntent = (message, session_attributes = {}) => {
    const dialogResponse = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': {
                'contentType': 'CustomPayload',
                'content': message
            },
            'responseCard': {
                'version': 1,
                'contentType': 'application/vnd.amazonaws.card.generic',
                'genericAttachments': [{
                    'buttons': [
                        { text: 'Click here', value: 'Clicked here for updated address' },
                    ]
                }]
            }
        }
    };
    return dialogResponse;
};
//Buttons to display Year make and model of the vehicle
async function buttonsForMakeModelYear(stepFnResponse, slotsData) {
    const dialogResponse = {
        'sessionAttributes': {
            stepFnRes: JSON.stringify(stepFnResponse),
            slots: JSON.stringify(slotsData),
        },
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': {
                'contentType': 'CustomPayload',
                'content': "Please Click on the make model and year of the vehicle you want to place the order for"
            },

        }
    };
    const responseCard = {
        'version': 1,
        'contentType': 'application/vnd.amazonaws.card.generic',
        'genericAttachments': []
    };
    console.log("========stepFnResponse==========butons===", JSON.stringify(stepFnResponse));
    const filteredData = stepFnResponse.Policies.filter((data) => typeof data == "object" && data["dssEnrollment"] == true);
    const buttons = filteredData.map((data) => ({
        text: `${data.modelYearNumber}, ${data.makeName}, ${data.modelName} `,
        value: `This is the Year Make Model VIN Number and Policy Number selected, ${data.makeName},${data.modelName},${data.modelYearNumber},${data.vinNumber},${data.agreDsplyNum}`,
    }))
        ;

    const totalFourQuestions = buttons.slice(0, 4);
    console.log("totalFourQuestions buttons are ", totalFourQuestions);

    totalFourQuestions.push(
        {
            text: 'Vehicle Not listed',
            value: 'NotListed'
        });

    console.log("Question buttons are ", totalFourQuestions);
    // Create the generic attachment to add to the lex response.
    const attachment = {
        'buttons': totalFourQuestions
    };
    console.log("The generic attachment is : ", attachment);

    responseCard['genericAttachments'].push(attachment);
    dialogResponse['dialogAction']['responseCard'] = responseCard;
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}

async function buttonsForMakeModelYearNoDiscount(stepFnResponse, slotsData) {
    const dialogResponse = {
        'sessionAttributes': {
            stepFnRes: JSON.stringify(stepFnResponse),
            slots: JSON.stringify(slotsData),
        },
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': {
                'contentType': 'CustomPayload',
                'content': "Please select the vehicle that needs the Drive Safe and Save (TM) discount"
            },

        }
    };
    const responseCard = {
        'version': 1,
        'contentType': 'application/vnd.amazonaws.card.generic',
        'genericAttachments': []
    };
    console.log("========stepFnResponse==========butons===", JSON.stringify(stepFnResponse));
    const filteredData = stepFnResponse.Policies.filter((data) => typeof data == "object" && data["dssEnrollment"] == false);
    const buttons = filteredData.map((data) => ({
        text: `${data.modelYearNumber}, ${data.makeName}, ${data.modelName}`,
        value: `List of No Discount Vehicles should will be shown, ${data.makeName},${data.modelName},${data.modelYearNumber},${data.vinNumber},${data.agreDsplyNum}`,
    }))
        ;

    const totalFourQuestions = buttons.slice(0, 4);
    console.log("totalFourQuestions buttons are ", totalFourQuestions);

    totalFourQuestions.push(
        {
            text: 'Vehicle Not listed',
            value: 'Vehicle is not present in the overall vehicle list'
        });

    console.log("Question buttons are ", totalFourQuestions);
    // Create the generic attachment to add to the lex response.
    const attachment = {
        'buttons': totalFourQuestions
    };
    console.log("The generic attachment is : ", attachment);

    responseCard['genericAttachments'].push(attachment);
    dialogResponse['dialogAction']['responseCard'] = responseCard;
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}

//Buttons to display valid reasons to reorder beacon
async function buttonsForValidReasons(stepFnResponse, inputTranscript, slotsData) {
    const dialogResponse = {
        'sessionAttributes': {
            stepFnRes: JSON.stringify(stepFnResponse),
            inputData: JSON.stringify(inputTranscript),
            slots: JSON.stringify(slotsData)
        },
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': {
                'contentType': 'CustomPayload',
                'content': "Please select a Valid reason for the Drive Safe & Save(TM) Beacon Reorder"
            },

        }
    };
    const responseCard = {
        'version': 1,
        'contentType': 'application/vnd.amazonaws.card.generic',
        'genericAttachments': []
    };
    const questions = [
        { text: 'Sent to wrong address', value: 'Sent to wrong address' },
        { text: 'No red light when button held down', value: 'No red light when button held down' },
        { text: 'Left in prior vehicle/Re-enrollment', value: 'Left in prior vehicle/Re-enrollment' },
        { text: 'Misplaced / Stolen', value: 'Misplaced / Stolen' },
        { text: 'None of the above', value: 'None of the above' },
    ];
    console.log("Question buttons are ", questions);
    // Create the generic attachment to add to the lex response.
    const attachment = {
        'buttons': questions
    };
    console.log("The generic attachment is : ", attachment);

    responseCard['genericAttachments'].push(attachment);
    dialogResponse['dialogAction']['responseCard'] = responseCard;
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}

// ---------------------------------------------------- Setting up Step function connection. ------------------------------------------ //
//this function will get the output from step function
let counter = 0;

async function delay(ms) {
    // return await for better async stack trace support in case of errors.
    return await new Promise(resolve => setTimeout(resolve, ms));
}

async function describeStepExecution(executionResult) {
    console.log("========executionResult=============", executionResult);
    const activityTaskReq = {
        executionArn: executionResult["executionArn"],
    };

    const stepFnResponse = await stepFunctions.describeExecution(activityTaskReq).promise();
    console.log("========stepFnResponse=============", JSON.stringify(stepFnResponse));
    if (stepFnResponse["status"] == "RUNNING" && counter < 12) {
        counter++;
        await delay(1000);
        return await describeStepExecution(executionResult);
    } else {
        return stepFnResponse;
    }
}

//this function will start the step function
async function startStepFnExecution(inputData) {
    try {
        const params = {
            stateMachineArn: stepFn,
            input: JSON.stringify({ birthDate: inputData.dateOfbirth, phone: inputData.phoneNumber }),
        };
        //startinghere step function
        const startExecution = await stepFunctions.startExecution(params).promise();
        console.log("========executionResult=============", startExecution);
        const describeStepExecutionResponse = await describeStepExecution(startExecution);
        console.log("counter", counter);
        counter = 0;
        console.log(" counter after", counter);
        console.log("========describeStepExecutionResponse=============", JSON.stringify(describeStepExecutionResponse));
        const finalResponse = {
            status: true,
            data: describeStepExecutionResponse
        };
        return finalResponse;
    } catch (err) {
        console.log("Error While Executing Step Function", err);
        const finalResponse = {
            status: false,
            data: err
        };
        return finalResponse;
    }
}

//This function will start execution
async function executeStateMachine(intent_request) {
    const slots = intent_request['currentIntent']['slots'];
    const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
                    Phone Number: ${slots["phoneNumber"]},\n
                    Birth Date: ${slots["dateOfbirth"]}`;
    const stepFnResponse = await startStepFnExecution(slots);

    if (stepFnResponse["status"] && stepFnResponse["data"]["status"] == "SUCCEEDED") {
        const finalStepFn = JSON.parse(stepFnResponse["data"]["output"]);
        console.log("======finalStepFn=========", typeof finalStepFn);
        const finalData = typeof finalStepFn == "string" ? close('Fulfilled', message) :
            await buttonsForMakeModelYear(finalStepFn, slots);
        return finalData;
    } else {
        return close('Fulfilled', message);
    }
}

// ----------------------------------------- Functions that cont(rol the bot's behavior --------------------------------------- //
//This function will control the Customer Data intent behaviour of bot
async function customerData(intent_request) {
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    const slots = intent_request['currentIntent']['slots'];
    const phonenumber = intent_request['currentIntent']['slots']['phoneNumber'];
    const dateofbirth = intent_request['currentIntent']['slots']['dateOfbirth'];
    console.log("outside  part", intent_request['currentIntent']['slots']);
    if (phonenumber != null && dateofbirth != null) {
        console.log("inside the phonenumber and dob is not null part");
        const filteredNumber = phonenumber.replace(/[^\d]/g, '');
        console.log("filteredNumber", filteredNumber);
        if (filteredNumber.length == 10) {
            console.log("=======filteredNumber length is 10==========", filteredNumber);
            intent_request['currentIntent']['slots']['phoneNumber'] = filteredNumber;
        } else {
            console.log("=======filteredNumber length is > 10==========", filteredNumber);
            const finalNumber = filteredNumber.substr(1);
            intent_request['currentIntent']['slots']['phoneNumber'] = finalNumber;
        }
        console.log("phonenumber", intent_request['currentIntent']['slots']['phoneNumber']);

        return await executeStateMachine(intent_request);
    } else if (phonenumber != null) {
        console.log("inside the phonenumber is not null part", intent_request['currentIntent']['slots']);
        return elicitSlot(session_attributes, intentName, slots, 'dateOfbirth');
    } else if (phonenumber == null) {
        console.log("inside the phonenumber null part", intent_request['currentIntent']['slots']);
        return elicitSlot(session_attributes, intentName, slots, 'phoneNumber');
    }
}

//This function will control the year make modelintent behaviour of bot
async function yearMakeModel(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const sessionKeys = Object.keys(session_attributes);
    const sessionKeysLength = sessionKeys.length;
    if (sessionKeysLength > 0) {
        const inputData = intent_request["inputTranscript"];
        const slotsData = JSON.parse(session_attributes['slots']);
        const finalInputData = inputData.split(",");
        const stepFnResponse = JSON.parse(session_attributes['stepFnRes']);
        console.log("======stepFnResponse============", JSON.stringify(stepFnResponse));
        return await buttonsForValidReasons(stepFnResponse, finalInputData, slotsData);
    } else {
        intent_request['currentIntent']['name'] = "CCC_IS_CustomerData_Intent";
        return await customerData(intent_request);
    }

}

async function yearMakeModelForNotListed(intent_request) {
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    const sessionKeys = Object.keys(session_attributes);
    const sessionKeysLength = sessionKeys.length;
    const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];
    console.log("======yearMakeModelForNotListed=========sessionKeys===", sessionKeys);
    if (confirmationStatus == 'None') {
        session_attributes["inputData"] = intent_request["inputTranscript"];
        console.log("======yearMakeModelForNotListed=========session_attributes===", session_attributes);
        const dialogResponse = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'ConfirmIntent',
                'intentName': intentName,
                'message': {
                    'contentType': 'CustomPayload',
                    'content': 'Has this vehicle had the Drive Safe and Save (TM) discount before?.'
                },
                'responseCard': {
                    'version': 1,
                    'contentType': 'application/vnd.amazonaws.card.generic',
                    'genericAttachments': [{
                        'buttons': [
                            { text: 'Yes', value: 'Yes' },
                            { text: 'No', value: 'No' },
                        ]
                    }]
                }
            }
        };

        console.log("The dialogResponse will be formatted as : ", dialogResponse);
        return dialogResponse;
    } else if (confirmationStatus == 'Denied') {
        console.log("======Denied=========invoked===");
        const message = 'Please add the Drive Safe and Save (TM) discount to the policy.  Doing this will automatically ship the Drive Safe and Save (TM) discount beacon.';
        return close('Fulfilled', message, session_attributes);
    }
    else if (confirmationStatus == 'Confirmed') {
        console.log("======Confirmed=========invoked===");
        if (sessionKeysLength > 0) {
            // const inputDataFromAttr = JSON.parse(session_attributes['inputData']);
            const slotsData = JSON.parse(session_attributes['slots']);
            const inputData = session_attributes['inputData'].split(",");
            const stepFnResponse = JSON.parse(session_attributes['stepFnRes']);
            console.log("======stepFnResponse============", JSON.stringify(stepFnResponse));
            console.log("====slotsData===========", slotsData, typeof slotsData, inputData);
            const mailingAddr = `${stepFnResponse['MailingAddress']['addressLines']} ${stepFnResponse['MailingAddress']['city']} 
            ${stepFnResponse['MailingAddress']['stateProvince']} ${stepFnResponse['MailingAddress']['postalCode']}`;
            const customerName = `${stepFnResponse['Name']['firstName']} ${stepFnResponse['Name']['middleName']} ${stepFnResponse['Name']['lastName']}`;
            const customerPhoneNumber = slotsData["phoneNumber"];
            const year = inputData[3];
            const make = inputData[1];
            const model = inputData[2];
            const vinNumber = inputData[4];
            const agreDsplyNum = inputData[5];

            const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
                            ClientName: ${customerName}, \n
                            PhoneNumber: ${customerPhoneNumber}, \n
                            Address:${mailingAddr}, \n
                            VIN: ${vinNumber}, \n
                            PolicyNumber:${agreDsplyNum}, \n
                            Year, Make, Model: ${year}, ${make}, ${model}, \n
                            Reason to Re-order : Adding DSS discount for the vehicle`;
            return close('Fulfilled', message, session_attributes);
        } else {
            intent_request['currentIntent']['name'] = "CCC_IS_CustomerData_Intent";
            return await customerData(intent_request);
        }
    }
}

//This function will control the not listed intent behaviour of bot
async function notListed(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const sessionKeys = Object.keys(session_attributes);
    const sessionKeysLength = sessionKeys.length;
    if (sessionKeysLength > 0) {
        const slots = JSON.parse(session_attributes['slots']);
        const stepFnResponse = JSON.parse(session_attributes['stepFnRes']);
        // const mailingAddr = `${stepFnResponse['MailingAddress']['addressLines']} ${stepFnResponse['MailingAddress']['city']} 
        //  ${stepFnResponse['MailingAddress']['stateProvince']} ${stepFnResponse['MailingAddress']['postalCode']}`;
        // const customerName = `${stepFnResponse['Name']['firstName']} ${stepFnResponse['Name']['middleName']} ${stepFnResponse['Name']['lastName']}`;
        // const customerPhoneNumber = slots["phoneNumber"];
        // const customerBirthDate = slots["dateOfbirth"];
        // const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
        //                  Client Name: ${customerName}, \n
        //                  Address: ${mailingAddr}, \n
        //                  Phone Number: ${customerPhoneNumber}, \n
        //                  Birth Date: ${customerBirthDate}`;

        // return close('Fulfilled', message, session_attributes);

        return await buttonsForMakeModelYearNoDiscount(stepFnResponse, slots);
    } else {
        intent_request['currentIntent']['name'] = "CCC_IS_CustomerData_Intent";
        return await customerData(intent_request);
    }
}

//This function will control the Sent to Wrong Addrress intent behaviour of bot
async function wrongAddressData(intent_request) {
    console.log("==========session_attributes=======", JSON.stringify(intent_request["sessionAttributes"]));
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    const sessionKeys = Object.keys(session_attributes);
    const sessionKeysLength = sessionKeys.length;
    if (sessionKeysLength > 0) {
        const slots = JSON.parse(session_attributes['slots']);
        console.log("====slots===========", slots, typeof slots);
        const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];
        const stepFnResponse = JSON.parse(session_attributes['stepFnRes']);
        const inputData = JSON.parse(session_attributes['inputData']);
        console.log("inpuT Data", inputData);
        const mailingAddr = `${stepFnResponse['MailingAddress']['addressLines']}, ${stepFnResponse['MailingAddress']['city']}, ${stepFnResponse['MailingAddress']['stateProvince']}, ${stepFnResponse['MailingAddress']['postalCode']}`;
        const customerName = `${stepFnResponse['Name']['firstName']} ${stepFnResponse['Name']['middleName']} ${stepFnResponse['Name']['lastName']}`;
        const customerPhoneNumber = slots["phoneNumber"];

        const year = inputData[3];
        const make = inputData[1];
        const model = inputData[2];
        const vinNumber = inputData[4];
        const agreDsplyNum = inputData[5];

        if (confirmationStatus == 'None') {
            const dialogResponse = {
                'sessionAttributes': session_attributes,
                'dialogAction': {
                    'type': 'ConfirmIntent',
                    'intentName': intentName,
                    'message': {
                        'contentType': 'CustomPayload',
                        'content': `Is this the right mailing address ${mailingAddr}.`
                    },
                    'responseCard': {
                        'version': 1,
                        'contentType': 'application/vnd.amazonaws.card.generic',
                        'genericAttachments': [{
                            'buttons': [
                                { text: 'Yes', value: 'Yes' },
                                { text: 'No', value: 'No' },
                            ]
                        }]
                    }
                }
            };

            console.log("The dialogResponse will be formatted as : ", dialogResponse);
            return dialogResponse;
        } else if (confirmationStatus == 'Denied') {
            /*const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
                             ClientName: ${customerName}, \n
                            PhoneNumber: ${customerPhoneNumber}, \n
                            VIN: ${vinNumber}, \n
                            PolicyNumber: ${agreDsplyNum}, \n
                            Year, Make, Model: ${year}, ${make}, ${model}, \n
                            Reason to Re-order : Sent to Wrong Address`
                ;*/
            const message = `We are unable to ship a beacon to any address other than the primary client address.Temporary, military, and foreign addresses cannot be used. Please update the primary client address in ECRM and Click on below button after ECRM has been updated`;
            return ElicitIntent(message, session_attributes);
        }
        else if (confirmationStatus == 'Confirmed') {
            const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
                        ClientName: ${customerName}, \n
                        PhoneNumber: ${customerPhoneNumber}, \n
                        Address: ${mailingAddr}, \n
                        VIN: ${vinNumber}, \n
                        PolicyNumber: ${agreDsplyNum}, \n
                        Year, Make, Model: ${year}, ${make}, ${model}, \n
                        Reason to Re-order : Sent to Wrong Address, Address verified`
                ;
            return close('Fulfilled', message, session_attributes)
        }
    } else {
        intent_request['currentIntent']['name'] = "CCC_IS_CustomerData_Intent";
        return await customerData(intent_request);
    }
}

//This function will control the No red Light intent behaviour of bot
async function noRedLight(intent_request) {
    //fix for seperating final display text based on button clicked.
    const inputTranscript = intent_request['inputTranscript'];

    const session_attributes = intent_request["sessionAttributes"];
    const sessionKeys = Object.keys(session_attributes);
    const sessionKeysLength = sessionKeys.length;
    if (sessionKeysLength > 0) {
        const slots = JSON.parse(session_attributes['slots']);
        console.log("====slots===========", slots, typeof slots);
        const stepFnResponse = JSON.parse(session_attributes['stepFnRes']);
        const inputData = JSON.parse(session_attributes['inputData']);
        const mailingAddr = `${stepFnResponse['MailingAddress']['addressLines']} ${stepFnResponse['MailingAddress']['city']} 
        ${stepFnResponse['MailingAddress']['stateProvince']} ${stepFnResponse['MailingAddress']['postalCode']}`;
        const customerName = `${stepFnResponse['Name']['firstName']} ${stepFnResponse['Name']['middleName']} ${stepFnResponse['Name']['lastName']}`;
        const customerPhoneNumber = slots["phoneNumber"];
        const year = inputData[3];
        const make = inputData[1];
        const model = inputData[2];
        const vinNumber = inputData[4];
        const agreDsplyNum = inputData[5];

        const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
                        ClientName: ${customerName}, \n
                        PhoneNumber: ${customerPhoneNumber}, \n
                        Address:${mailingAddr}, \n
                        VIN: ${vinNumber}, \n
                        PolicyNumber:${agreDsplyNum}, \n
                        Year, Make, Model: ${year}, ${make}, ${model}, \n
                        Reason to Re-order : ${inputTranscript}`;
        return close('Fulfilled', message, session_attributes);
    } else {
        intent_request['currentIntent']['name'] = "CCC_IS_CustomerData_Intent";
        return await customerData(intent_request);
    }
}

//This function will control the None of the above intent behaviour of bot
async function noneOfAbove(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const sessionKeys = Object.keys(session_attributes);
    const sessionKeysLength = sessionKeys.length;
    if (sessionKeysLength > 0) {
        const slots = JSON.parse(session_attributes['slots']);
        console.log("====slots===========", slots, typeof slots);
        const stepFnResponse = JSON.parse(session_attributes['stepFnRes']);
        const inputData = JSON.parse(session_attributes['inputData']);
        const mailingAddr = `${stepFnResponse['MailingAddress']['addressLines']} ${stepFnResponse['MailingAddress']['city']} 
        ${stepFnResponse['MailingAddress']['stateProvince']} ${stepFnResponse['MailingAddress']['postalCode']}`;
        const customerName = `${stepFnResponse['Name']['firstName']} ${stepFnResponse['Name']['middleName']} ${stepFnResponse['Name']['lastName']}`;
        const customerPhoneNumber = slots["phoneNumber"];
        const year = inputData[3];
        const make = inputData[1];
        const model = inputData[2];
        const vinNumber = inputData[4];
        const agreDsplyNum = inputData[5];

        const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
                        ClientName: ${customerName}, \n
                        Address: ${mailingAddr}, \n
                        PhoneNumber: ${customerPhoneNumber}, \n
                        VIN: ${vinNumber}, \n
                        PolicyNumber:${agreDsplyNum}, \n
                        Year, Make, Model: ${year}, ${make}, ${model}, \n
                        Reason to Re-order : None of the Above `;
        return close('Fulfilled', message, session_attributes);
    } else {
        intent_request['currentIntent']['name'] = "CCC_IS_CustomerData_Intent";
        return await customerData(intent_request);
    }

}

async function transferToRep(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const sessionKeys = Object.keys(session_attributes);
    const sessionKeysLength = sessionKeys.length;
    if (sessionKeysLength > 0) {
        const slots = JSON.parse(session_attributes['slots']);
        console.log("====slots===========", slots, typeof slots);
        const stepFnResponse = JSON.parse(session_attributes['stepFnRes']);
        //const inputData = JSON.parse(session_attributes['inputData']);
        const mailingAddr = `${stepFnResponse['MailingAddress']['addressLines']} ${stepFnResponse['MailingAddress']['city']} 
        ${stepFnResponse['MailingAddress']['stateProvince']} ${stepFnResponse['MailingAddress']['postalCode']}`;
        const customerName = `${stepFnResponse['Name']['firstName']} ${stepFnResponse['Name']['middleName']} ${stepFnResponse['Name']['lastName']}`;
        const customerPhoneNumber = slots["phoneNumber"];

        const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
                        ClientName: ${customerName}, \n
                        Address: ${mailingAddr}, \n
                        PhoneNumber: ${customerPhoneNumber}, \n
                        Reason to Re-order : Vehicle not found`;
        return close('Fulfilled', message, session_attributes);
    } else {
        intent_request['currentIntent']['name'] = "CCC_IS_CustomerData_Intent";
        return await customerData(intent_request);
    }

}

async function repullingAddress(intent_request) {
    console.log("==========session_attributes=======", JSON.stringify(intent_request["sessionAttributes"]));
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    const sessionKeys = Object.keys(session_attributes);
    const sessionKeysLength = sessionKeys.length;
    const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];
    if (sessionKeysLength > 0) {
        const slots = JSON.parse(session_attributes["slots"]);
        const inputData = JSON.parse(session_attributes['inputData']);
        const stepFnResponse = JSON.parse(session_attributes['stepFnRes']);
        const slotsData = JSON.parse(session_attributes['slots']);
        const year = inputData[3];
        const make = inputData[1];
        const model = inputData[2];
        const vinNumber = inputData[4];
        const agreDsplyNum = inputData[5];
        const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
                            Phone Number: ${slots["phoneNumber"]},\n
                            Birth Date: ${slots["dateOfbirth"]}\n                        
                            VIN: ${vinNumber}, \n
                            PolicyNumber: ${agreDsplyNum}, \n
                            Year, Make, Model: ${year}, ${make}, ${model}, \n`;
        const mailingAddr = `${stepFnResponse['MailingAddress']['addressLines']} ${stepFnResponse['MailingAddress']['city']} 
                            ${stepFnResponse['MailingAddress']['stateProvince']} ${stepFnResponse['MailingAddress']['postalCode']}`;
        const customerName = `${stepFnResponse['Name']['firstName']} ${stepFnResponse['Name']['middleName']} ${stepFnResponse['Name']['lastName']}`;
        const customerPhoneNumber = slotsData["phoneNumber"];
        if (confirmationStatus == 'None') {
            const stepFunctionRes = await startStepFnExecution(slots);

            if (stepFunctionRes["status"] && stepFunctionRes["data"]["status"] == "SUCCEEDED") {
                const finalStepFn = JSON.parse(stepFunctionRes["data"]["output"]);
                console.log("======finalStepFn=========", JSON.stringify(finalStepFn));
                session_attributes["stepFnRes"] = JSON.stringify(finalStepFn);
                const newMailingAddr = `${finalStepFn['MailingAddress']['addressLines']} ${finalStepFn['MailingAddress']['city']} 
                ${finalStepFn['MailingAddress']['stateProvince']} ${finalStepFn['MailingAddress']['postalCode']}`;
                const messageForConfirmIntent = `Is this the right mailing address ${newMailingAddr}`;
                const finalData = typeof finalStepFn == "string" ? close('Fulfilled', message) :
                    await confirmIntent(intentName, messageForConfirmIntent, session_attributes);
                return finalData;
            } else {
                return close('Fulfilled', message);
            }
        }
        else if (confirmationStatus == 'Denied') {
            const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
            ClientName: ${customerName}, \n
            PhoneNumber: ${customerPhoneNumber}, \n
            VIN: ${vinNumber}, \n
            PolicyNumber: ${agreDsplyNum}, \n
            Year, Make, Model: ${year}, ${make}, ${model}, \n
            Reason to Re-order : Sent to Wrong Address, Address not verified after updating the ECRM`
                ;
            return close('Fulfilled', message, session_attributes);
        }
        else if (confirmationStatus == 'Confirmed') {
            const stepFnResponse = JSON.parse(session_attributes['stepFnRes']);
            const newMailingAddr = `${stepFnResponse['MailingAddress']['addressLines']} ${stepFnResponse['MailingAddress']['city']} 
            ${stepFnResponse['MailingAddress']['stateProvince']} ${stepFnResponse['MailingAddress']['postalCode']}`;
            const message = `Thank you, I am connecting you to a representative to re-order the  Drive Safe & Save(TM) Beacon for \n
                    ClientName: ${customerName}, \n
                    PhoneNumber: ${customerPhoneNumber}, \n
                    Address: ${newMailingAddr}, \n
                    VIN: ${vinNumber}, \n
                    PolicyNumber: ${agreDsplyNum}, \n
                    Year, Make, Model: ${year}, ${make}, ${model}, \n
                    Reason to Re-order : Sent to Wrong Address, Address Updated in ECRM`
                ;
            return close('Fulfilled', message, session_attributes);
        }
    } else {
        intent_request['currentIntent']['name'] = "CCC_IS_CustomerData_Intent";
        return await customerData(intent_request);
    }
}

// ------------------------------------------ Dispatch Intents ---------------------------------------------- //

async function dispatch(intent_request) {
    console.log("====intent_request======", JSON.stringify(intent_request));

    const intent_name = intent_request['currentIntent']['name'];
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'CCC_IS_CustomerData_Intent') {
        return await customerData(intent_request);
    }
    else if (intent_name == 'CCC_IS_YearMakeModel_Intent') {
        return await yearMakeModel(intent_request);
    }
    else if (intent_name == 'CCC_IS_WrongAddress_Intent') {
        return await wrongAddressData(intent_request);
    }
    else if (intent_name == 'CCC_IS_NotListed_Intent') {
        return await notListed(intent_request);
    }
    else if (intent_name == 'CCC_IS_NoRedLight_Intent') {
        return await noRedLight(intent_request);
    }
    else if (intent_name == 'CCC_IS_NoneOfAbove_Intent') {
        return await noneOfAbove(intent_request);
    }
    else if (intent_name == 'CCC_IS_NoDiscountVehicles_Intent') {
        return await yearMakeModelForNotListed(intent_request);
    }
    else if (intent_name == 'CCC_IS_TrasnferToRep_intent') {
        return await transferToRep(intent_request);
    }
    else if (intent_name == 'CCC_IS_RepullingAddress_Intent') {
        return await repullingAddress(intent_request);
    }
}

exports.handler = async (event) => {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
};
