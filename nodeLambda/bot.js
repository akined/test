'use strict'
const AWS = require('aws-sdk');

// ---------------------------------------------------- DynamoDb connection set up. ------------------------------------------ //
const dynamodb = new AWS.DynamoDB({});
const dynamoTable = 'sf-conncen-test-podd-pcassistantbot_DB';

const descriptors = ['L', 'M', 'N', 'S', 'B', 'SS', 'NULL', 'NS', 'BS'];
const flattenDynamoDbResponse = (o) => {

    // flattens single property objects that have descriptors  
    for (let d of descriptors) {
        if (o.hasOwnProperty(d)) {
            return o[d];
        }
    }

    Object.keys(o).forEach((k) => {

        for (let d of descriptors) {
            if (o[k].hasOwnProperty(d)) {
                o[k] = o[k][d];
            }
        }
        if (Array.isArray(o[k])) {
            o[k] = o[k].map(e => flattenDynamoDbResponse(e))
        } else if (typeof o[k] === 'object') {
            o[k] = flattenDynamoDbResponse(o[k])
        }
    });

    return o;
}

async function get_response(intent_name) {
    try {
        // Get the FAQ respnose for given intent_name.

        const intentResponse = {}
        const params = {
            Key: {
                'IntentName': {
                    S: intent_name
                }
            },
            TableName: dynamoTable
        };
        const dynamodbResponse = await dynamodb.getItem(params).promise();
        const response = flattenDynamoDbResponse(dynamodbResponse);

        intentResponse["Response"] = response['Item']['FAQResponse'];
        intentResponse["RelatedQuestions"] = response['Item']['RelatedQuestions'];

        console.log("The related questions are as follows: ", intentResponse["RelatedQuestions"]);
        console.log("The intent resopnse is :", intentResponse);

        // Finally return the composed intent response to delegate.
        return intentResponse;
    } catch (err) {
        console.log("Error occured while trying to retrieve response for intent: ", intent_name);
        return err.response['Error']['Message'];
    }
}

// ---------------- Helpers to build responses which match the structure of the necessary dialog actions ----------------------- //

co nst get_slots = (intent_request) => intent_request['currentIntent']['slots'];

function elicitSlot(sessionAttributes, intentName, slots, slotToElicit, message) {
    console.log("Got to Elicit Slot Function");
    console.log("elicitSlot SessionAttributes: " + JSON.stringify(sessionAttributes));
    console.log("elicitSlot IntentName: " + JSON.stringify(intentName));
    console.log("elicitSlot Slots: " + JSON.stringify(slots));
    console.log("elicitSlot Slot to Elicit: " + JSON.stringify(slotToElicit));
    console.log("elicitSlot Message: " + JSON.stringify(message));

    return {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,

        },
    };
}

function confirmIntent(sessionAttributes, intentName, slots, message) {
    // add to transcript
    return {
        sessionAttributes,
        dialogAction: {
            type: 'ConfirmIntent',
            intentName,
            slots,
            message,
        },
    };
}

function close(sessionAttributes, fulfillmentState, message) {
    return {
        sessionAttributes,
        dialogAction: {
            type: 'Close',
            fulfillmentState,
            message,
        },
    };
}

const delegate = (fulfillment_state, message, related_intents = [], intent_request = {}) => {
    const dialogResponse = {
        'dialogAction': {
            'type': 'ElicitSlot',
            // 'fulfillmentState': fulfillment_state,
            'intentName': intent_request['currentIntent']['name'],
            'message': {
                'contentType': 'CustomPayload',
                'content': message
            },
            'slots': intent_request['currentIntent']['slots'],
            'slotToElicit': Object.keys(intent_request['currentIntent']['slots'])[0],
        }
    }
    // Check if we are given a list of related questions for this intent.
    if (related_intents.length > 0) {
        // Generate the response card to be added to dialogResponse.
        const responseCard = {
            'version': 1,
            'contentType': 'application/vnd.amazonaws.card.generic',
            'genericAttachments': []
        }
        const questions = []
        // Create buttons for each of the realted intent question.
        related_intents.map((question) => {
            const question_button = {
                'text': question,
                'value': question
            };
            questions.push(question_button);
        });

        console.log("Question buttons are ", questions);
        // Create the generic attachment to add to the lex response.
        const attachment = {
            'title': 'You can also ask..',
            'buttons': questions
        };
        console.log("The generic attachment is : ", attachment);
        // Attach the attachments to the response card.
        responseCard['genericAttachments'].push(attachment)
        // Attach response card to dialogResponse.
        dialogResponse['dialogAction']['responseCard'] = responseCard
        console.log("The dialogResponse will be formatted as : ", dialogResponse);
    }
    return dialogResponse
}


// ---------------------------------------------- Validation Helper Functions ---------------------------------------------- //

const build_validation_result = (is_valid, violated_slot, message_content) => {
    if (message_content) {
        return {
            "isValid": is_valid,
            "violatedSlot": violated_slot,
        }
    } else {
        return {
            'isValid': is_valid,
            'violatedSlot': violated_slot,
            'message': { 'contentType': 'CustomPayload', 'content': message_content }
        }
    }
};

const validate_policy_type = (policy_type) => {
    const policy_types = ['commercial auto', 'auto', 'business auto', 'auto application', 'auto app', 'commercial fire', 'fire', 'business'];
    if (policy_type && !policy_types.includes(policy_type.toString().toLowerCase())) {
        return build_validation_result(false,
            'policyType',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.');
    } else {
        return build_validation_result(true, '', '');
    }
}
const validate_vehicle_type = (vehicle_type) => {
    return build_validation_result(true, '', '')
};

const validate_document_type = (document_type) => {
    const document_types = ['selection rejection', 'sel/rej', 'sel rej', 'selrej', 'um form', 'uninsured form', 'u form',
        'u-form', 'underinsured form', 'uninsured motorist form', 'uninsures mostorist form', 'selection / rejection forms', 'uform', 'select reject'];
    if (document_type && !document_types.includes(document_type.toString().toLowerCase())) {
        return build_validation_result(false,
            'document',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.')
    } else {
        return build_validation_result(true, '', '');
    }
};

const validate_line_coverage = (policy_type, coverage_type) => {
    const policy_types = ['commercial auto', 'auto', 'business auto', 'auto application', 'auto app', 'commercial fire', 'fire', 'business'];
    if (policy_type && !policy_types.includes(policy_type.toString().toLowerCase())) {
        return build_validation_result(false,
            'policyType',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.');
    } else {
        return build_validation_result(true, '', '')
    }
};

const validate_vehicle_coverage = (coverage_type) => {
    return build_validation_result(true, '', '')
};

const validate_take_payment = (payments_info) => {
    const payments_infos = ['Payment', 'bill', 'how much', 'billing', 'sf billing', 'owe', 'nonpay', 'take'];
    if (payments_info && !payments_infos.includes(payments_info.toString().toLowerCase())) {
        return build_validation_result(false,
            'payment_info',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.');
    } else {
        return build_validation_result(true, '', '')
    }
};
const validate_care_form = (care_form) => {
    const care_forms = ['CARE form', 'CARE', 'form', 'where', 'ABS', 'REPLACE'];
    if (care_form && !care_forms.includes(care_form.toString().toLowerCase())) {
        return build_validation_result(false,
            'care_form',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.');
    } else {
        return build_validation_result(true, '', '')
    }
};

const validate_upload_type = (upload_type) => {
    const upload_types = ['upload', 'attach', 'migrate', 'signed document'];
    if (upload_type && !upload_types.includes(upload_type.toString().toLowerCase())) {
        return build_validation_result(false,
            'care_form',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.');
    } else {
        return build_validation_result(true, '', '')
    }
};

// ----------------------------------------- Functions that control the bot's behavior --------------------------------------- //

async function take_payment(intent_request) {
    const payments_info = get_slots(intent_request)["payment_information"]
    const validation_result = validate_take_payment(payments_info)
    if (!validation_result['isValid']) {
        const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
}
async function careform_in_abs(intent_request) {
    const care_form = get_slots(intent_request)["care_form"]
    const validation_result = validate_care_form(care_form)
    if (!validation_result['isValid']) {
        const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
}
async function access_policycenter(intent_request) {
    const care_form = get_slots(intent_request)["care_form"]
    const validation_result = validate_care_form(care_form)
    if (!validation_result['isValid']) {
        const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
}
async function billing_answers(intent_request) {
    const care_form = get_slots(intent_request)["care_form"]
    const validation_result = validate_care_form(care_form)
    if (!validation_result['isValid']) {
        const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
}
async function extend_workspace(intent_request) {
    const care_form = get_slots(intent_request)["care_form"]
    const validation_result = validate_care_form(care_form)
    if (!validation_result['isValid']) {
        const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
}

async function upload_documents(intent_request) {
    const upload_type = get_slots(intent_request)["upload_type"]
    const validation_result = validate_upload_type(upload_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
}
async function submission_instruction(intent_request) {
    // Performs validations on policy type slot and then returns new submission instructions as needed.

    const policy_type = get_slots(intent_request)["policyType"]
    // Perform basic validation on the supplied input slots.
    const validation_result = validate_policy_type(policy_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
}

async function add_vehicle(intent_request) {

    // Provides instructions on adding a new vehicle to an auto policy.

    const vehicle_type = get_slots(intent_request)["vehicleType"]

    // Perform basic validation on the supplied input slots.
    const validation_result = validate_vehicle_type(vehicle_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        return delegate('Fulfilled', errorMessage);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('vehicle_type', vehicle_type), response["RelatedQuestions"], intent_request)
    }
}

async function generate_document(intent_request) {

    // Provides instructions on how to generate a new document.


    const document_type = get_slots(intent_request)["document"]

    //Perform basic validation on the supplied input slots.
    const validation_result = validate_document_type(document_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        return delegate('Fulfilled', errorMessage)
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('document_type', document_type), response["RelatedQuestions"])
    }
}

async function line_level_coverage(intent_request) {

    //Provides instructions on updating line level coverages.


    const coverage_type = get_slots(intent_request)["coverageType"]
    const policy_type = get_slots(intent_request)["policyType"]
    const state_coverages = ['uninsured motor vehicle', 'underinsured motor vehicle', 'state info']
    let screen_name = ['Commercial Auto Line']

    if (state_coverages.includes(coverage_type.toLowerCase())) {
        screen_name = 'State Info'

    }
    //(any(coverage_type.lower() in state_cov for state_cov in state_coverages)) 


    //Perform basic validation on the supplied input slots.
    const validation_result = validate_line_coverage(policy_type, coverage_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        if (validation_result['violatedSlot'] == 'policyType') {
            const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
            return delegate('Fulfilled', errorMessage)
        }
    }
    else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('coverage_type', coverage_type).replace('screen_name', screen_name), response["RelatedQuestions"])
    }
}

async function vehicle_level_coverage(intent_request) {

    //Provides instructions on updating vehicle level coverages.

    const coverage_type = get_slots(intent_request)["vehicleCoverage"]
    //Perform basic validation on the supplied input slots.
    const validation_result = validate_vehicle_coverage(coverage_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        if (validation_result['violatedSlot'] == 'vehicleCoverage') {
            const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
            return delegate('Fulfilled', errorMessage)
        }

    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('coverage_type', coverage_type), response["RelatedQuestions"])
    }
}

// ------------------------------------------ Dispatch Intents ---------------------------------------------- //

async function dispatch(intent_request) {
    //Called when the user specifies an intent for this bot.
    console.log(intent_request)

    const intent_name = intent_request['currentIntent']['name']
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'submission_instruction') {
        return await submission_instruction(intent_request);
    }
    else if (intent_name == 'add_vehicle') {
        return await add_vehicle(intent_request);
    } else if (intent_name == 'generate_document') {
        return await generate_document(intent_request);
    } else if (intent_name == 'line_level_coverage') {
        return await line_level_coverage(intent_request);
    } else if (intent_name == 'vehicle_level_coverage') {
        return await vehicle_level_coverage(intent_request);
    } else if (intent_name == 'take_payment') {
        return await take_payment(intent_request)
    } else if (intent_name == 'careform_in_abs') {
        return await careform_in_abs(intent_request)
    } else if (intent_name == 'access_policycenter') {
        return await access_policycenter(intent_request)
    } else if (intent_name == 'billing_answers') {
        return await billing_answers(intent_request)
    } else if (intent_name == 'extend_workspace') {
        return await extend_workspace(intent_request)
    } else if (intent_name == 'upload_documents') {
        return await upload_documents(intent_request)
    }
    else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return await delegate('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
};

// ---------------------------------------------------- Main handler ------------------------------------------------- //

exports.handler = async function (event, context) {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
};
