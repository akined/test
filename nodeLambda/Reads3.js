'use strict'
const AWS = require('aws-sdk');
var s3 = new AWS.S3({});
exports.handler = async function (event, context) 
{  try
    {   
        const s3Data = await s3.getObject({
           Bucket: event.Records[0].s3.bucket.name,
           Key: event.Records[0].s3.object.key,
            }).promise();
 const bufferDataToString = s3Data.Body.toString('utf-8');
 const sanitized = '[' + bufferDataToString.replace(/\}\s*\{/g, '},{') + ']';
const s3BucketData= JSON.parse(sanitized);
// Create the DynamoDB service object
    var ddb = new AWS.DynamoDB({ apiVersion: '2012-08-10' });
    const mappedData = s3BucketData.map((data) => 
    {const dynamoObj = {PutRequest: {	Item: JSON.parse(JSON.stringify({
        "ContactId": { S: data.ContactId },	
        "StartType": data.Attributes.StartType ? { S: data.Attributes.StartType } : undefined,
        "StartCondition": data.Attributes.StartCondition ? { S: data.Attributes.StartCondition } : undefined,
        "Task1": data.Attributes.Task1 ? { S: data.Attributes.Task1 } : undefined,
        "Result1": data.Attributes.Result1 ? { S: data.Attributes.Result1 } : undefined,
        "Task2": data.Attributes.Task2 ? { S: data.Attributes.Task2 } : undefined,
        "Result2": data.Attributes.Result2 ? { S: data.Attributes.Result2 } : undefined,
        "Task3": data.Attributes.Task3 ? { S: data.Attributes.Task3 } : undefined,
        "Result3": data.Attributes.Result3 ? { S: data.Attributes.Result3 } : undefined,
        "Task4": data.Attributes.Task4 ? { S: data.Attributes.Task4 } : undefined,
        "Result4": data.Attributes.Result4 ? { S: data.Attributes.Result4 } : undefined,
        "Task5": data.Attributes.Task5 ? { S: data.Attributes.Task5 } : undefined,
        "Result5": data.Attributes.Result5 ? { S: data.Attributes.Result5 } : undefined,
        "Task6": data.Attributes.Task6 ? { S: data.Attributes.Task6 } : undefined,
        "Result6": data.Attributes.Result6 ? { S: data.Attributes.Result6 } : undefined,
        "ExitType": data.Attributes.ExitType ? { S: data.Attributes.ExitType } : undefined,
        }))
        }
        };
        return dynamoObj;
        });
        
const params = {
    RequestItems: {
        'sf-conncen-rsch-ContactFlow_Attributes_DB': mappedData
        },
        }
        ;
// Call DynamoDB to add the item to the table
    const dynamoDBResponse = await ddb.batchWriteItem(params).promise();
            return dynamoDBResponse; 
        }catch (err) {
           console.log("err", err);
        return err;  
    }
};
