/*
* CCPLog size => 500KB for a 20 mins call.
* Cloudwatch pushEvents batch size 1MB per call.
* CloudWatch pushEvent can upload 1MB size of text at a time.
* This lambda can handle logs greater than 1MB and move it to cloudwatch (Internally slices logs of 1MB size).
* Max Event Size = 256KB
* Max Batch Size = 1MB
*/
const aws = require('aws-sdk');
const cloudWatchLogs = new aws.CloudWatchLogs({
    apiVersion: '2014-03-28'
});
const readline = require('readline');
const stream = require('stream');

let to = function (promise) {
   return promise.then(data => {
      return [null, data];
   })
   .catch(err => [err]);
};

let sliceString = function(str, len) {
  const size = Math.ceil(str.length/len);
  const slicedArr = Array(size);
  let offset = 0;

  for (let i = 0; i < size; i++) {
    slicedArr[i] = str.substr(offset, len);
    offset += len;
  }

  return slicedArr;
};

exports.handler = async (event, context, callback) => {
    const MAX_EVENT_SIZE = 261000; //262144 - 250(for Log formation text) - 26 (for LogEvent)
    let status = {"status":"Success"};
    let failureStatus = {"status":"Failure"};
    const response = {
        statusCode: 200,
        body: status
    };
    const failureResponse = {
        statusCode: 500,
        body: failureStatus
    };

    const logStreamName = event.AgentAlias; //Name of the log stream goes here - expecting agent loginID as a parameter from the client;
    const logGroupName = '/aws/connect/'+event.CCPInstance+'/CCP/'; //Name of the log group goes here - expecting the A-C instance name as a parameter from the client;
    var finalData = JSON.stringify(event.CCPLogs);
    const jsonData = sliceString(finalData, MAX_EVENT_SIZE);
    let logData = '';

    for (let i = 0; i < jsonData.length; i++) {
        logData = logData + new Date().toISOString('ascii') + '^ERROR Call Issue Reported - ContactId: '+ event.ContactId + ' Agent Alias: ' + event.AgentAlias + ' CallIssues: '+event.CallIssues + ' ' + jsonData[i] + '\n';
    }

    let [errManageLogStreams, manageLogStreamsData] = await to(manageLogStreams(logGroupName, logStreamName));

    if (errManageLogStreams || manageLogStreamsData.logStreams.length == 0) {
        let [errCreateLogStream] = await to(createLogStream(logGroupName, logStreamName));
        if(errCreateLogStream) console.error('error while creating log stream : '+errCreateLogStream);
        putLogEvents(null, logData, logGroupName, logStreamName);
    }else {
        putLogEvents(manageLogStreamsData.logStreams[0].uploadSequenceToken, logData, logGroupName, logStreamName);
    }
    await sleep();
    return response;
};

let manageLogStreams = function(logGroupName, logStreamName) {
    var describeLogStreamsParams = {
        logGroupName: logGroupName,
        logStreamNamePrefix: logStreamName
    };
    //check if the log stream already exists and get the sequenceToken
    return cloudWatchLogs.describeLogStreams(describeLogStreamsParams).promise();
};

let createLogStream = function(logGroupName, logStreamName) {
    var logStreamParams = {
        logGroupName: logGroupName,
        logStreamName: logStreamName
    };
    return cloudWatchLogs.createLogStream(logStreamParams).promise();
};

let putLogEvents = function(sequenceToken, logData, logGroupName, logStreamName) {
    //I stole this from http://docs.aws.amazon.com/AmazonCloudWatchLogs/latest/APIReference/API_PutLogEvents.html
    const MAX_BATCH_SIZE = 1048576; // 10/10/2018 - maximum size in bytes of Log Events (with overhead) per invocation of PutLogEvents
    const MAX_BATCH_COUNT = 10000; // 10/10/2018 - maximum number of Log Events per invocation of PutLogEvents
    const LOG_EVENT_OVERHEAD = 26; // WAG - bytes of overhead per Log Event
    // list of batches
    var batches = [];
    // list of events in current batch
    var batch = [];
    // size of events in the current batch
    var batch_size = 0;
    var bufferStream = new stream.PassThrough();
    bufferStream.end(logData);
    var rl = readline.createInterface({
                    input: bufferStream
    });
    var line_count = 0;
    rl.on('line', (line) => {
    ++line_count;
    var ts = line.split('^', 2)[0];
    var message = line.split('^', 2)[1];
    var tval = Date.parse(ts);
    var event_size = line.length + LOG_EVENT_OVERHEAD;
    batch_size += event_size;
    if (batch_size >= MAX_BATCH_SIZE ||
        batch.length >= MAX_BATCH_COUNT) {
        // start a new batch
        batches.push(batch);
        batch = [];
        batch_size = event_size;
    }
    batch.push({
        message: message,
        timestamp: tval
    });
    });
    rl.on('close', () => {
        // add the final batch
        batches.push(batch);
        sendBatches(sequenceToken, batches, logGroupName, logStreamName);
    });
};

function sendBatches(sequenceToken, batches, logGroupName, logStreamName) {
    var count = 0;
    var batch_count = 0;

    function sendNextBatch(err, nextSequenceToken, logGroupName, logStreamName) {
        if (err) {
            console.log('Error sending batch: ', err, err.stack);
            return;
        }
        else {
            var nextBatch = batches.shift();
            if (nextBatch) {
                // send this batch
                ++batch_count;
                count += nextBatch.length;
                sendBatch(nextSequenceToken, nextBatch, sendNextBatch, logGroupName, logStreamName);
            }
            else {
                // no more batches: it's miller time
                var msg = `Successfully put ${count} events in ${batch_count} batches`;
                console.log(msg);
                //callback(null, buildResponse(true));
            }
        }
    }
    sendNextBatch(null, sequenceToken, logGroupName, logStreamName);
}

let sleep = async function(){
	return new Promise(function(resolve) {
	   setTimeout(resolve, 1000);
	});
};

function sendBatch(sequenceToken, batch, doNext, logGroupName, logStreamName) {
    var putLogEventParams = {
        logEvents: batch,
        logGroupName: logGroupName,
        logStreamName: logStreamName
    };
    if (sequenceToken) {
        putLogEventParams['sequenceToken'] = sequenceToken;
    }
    // sort the events in ascending order by timestamp as required by PutLogEvents
    putLogEventParams.logEvents.sort(function(a, b) {
        if (a.timestamp > b.timestamp) {
            return 1;
        }
        if (a.timestamp < b.timestamp) {
            return -1;
        }
        return 0;
    });
    cloudWatchLogs.putLogEvents(putLogEventParams, function(err, data) {
        if (err) {
            console.log('Error during put log events: ', err, err.stack);
            doNext(err, null, null, null);
        }
        else {
            console.log(`Success in putting ${putLogEventParams.logEvents.length} events`);
            doNext(null, data.nextSequenceToken, logGroupName, logStreamName);
        }
    });
}
