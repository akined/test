'use strict'
const AWS = require('aws-sdk');

// ---------------------------------------------------- DynamoDb connection set up. ------------------------------------------ //
const dynamodb = new AWS.DynamoDB({});
const dynamoTable = 'sf-conncen-test-podd-pcassistantbot_DB';

const descriptors = ['L', 'M', 'N', 'S', 'B', 'SS', 'NULL', 'NS', 'BS'];
const flattenDynamoDbResponse = (o) => {

    // flattens single property objects that have descriptors  
    for (let d of descriptors) {
        if (o.hasOwnProperty(d)) {
            return o[d];
        }
    }

    Object.keys(o).forEach((k) => {

        for (let d of descriptors) {
            if (o[k].hasOwnProperty(d)) {
                o[k] = o[k][d];
            }
        }
        if (Array.isArray(o[k])) {
            o[k] = o[k].map(e => flattenDynamoDbResponse(e))
        } else if (typeof o[k] === 'object') {
            o[k] = flattenDynamoDbResponse(o[k])
        }
    });

    return o;
}

async function get_response(intent_name) {
    try {
        // Get the FAQ respnose for given intent_name.

        const intentResponse = {}
        const params = {
            Key: {
                'IntentName': {
                    S: intent_name
                }
            },
            TableName: dynamoTable
        };
        const dynamodbResponse = await dynamodb.getItem(params).promise();
        const response = flattenDynamoDbResponse(dynamodbResponse);

        intentResponse["Response"] = response['Item']['FAQResponse'];
        intentResponse["RelatedQuestions"] = response['Item']['RelatedQuestions'];

        console.log("The related questions are as follows: ", intentResponse["RelatedQuestions"]);
        console.log("The intent resopnse is :", intentResponse);

        // Finally return the composed intent response to delegate.
        return intentResponse;
    } catch (err) {
        console.log("Error occured while trying to retrieve response for intent: ", intent_name);
        return err.response['Error']['Message'];
    }
}

// ---------------- Helpers to build responses which match the structure of the necessary dialog actions ----------------------- //

const get_slots = (intent_request) => intent_request['currentIntent']['slots'];

const delegate = (fulfillment_state, message, related_intents = []) => {
    const dialogResponse = {
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': {
                'contentType': 'CustomPayload',
                'content': message
            }
        }
    }
    // Check if we are given a list of related questions for this intent.
    if (related_intents.length > 0) {
        // Generate tehthe response card to be added to dialogResponse.
        const responseCard = {
            'version': 1,
            'contentType': 'application/vnd.amazonaws.card.generic',
            'genericAttachments': []
        }
        const questions = []
        // Create buttons for each of the realted intent question.
        related_intents.map((question) => {
            const question_button = {
                'text': question,
                'value': question
            };
            questions.push(question_button);
        });

        console.log("Question buttons are ", questions);
        // Create the generic attachment to add to the lex response.
        const attachment = {
            'title': 'You can also ask..',
            'buttons': questions
        };
        console.log("The generic attachment is : ", attachment);
        // Attach the attachments to the response card.
        responseCard['genericAttachments'].push(attachment)
        // Attach response card to dialogResponse.
        dialogResponse['dialogAction']['responseCard'] = responseCard
        console.log("The dialogResponse will be formatted as : ", dialogResponse);
    }
    return dialogResponse
}


// ---------------------------------------------- Validation Helper Functions ---------------------------------------------- //

const build_validation_result = (is_valid, violated_slot, message_content) => {
    if (message_content) {
        return {
            "isValid": is_valid,
            "violatedSlot": violated_slot,
        }
    } else {
        return {
            'isValid': is_valid,
            'violatedSlot': violated_slot,
            'message': { 'contentType': 'CustomPayload', 'content': message_content }
        }
    }
};

const validate_policy_type = (policy_type) => {
    const policy_types = ['commercial auto', 'auto', 'business auto', 'auto application', 'auto app'];
    if (policy_type && !policy_types.includes(policy_type.toString().toLowerCase())) {
        return build_validation_result(false,
            'policyType',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.');
    } else {
        return build_validation_result(true, '', '');
    }
}
const validate_vehicle_type = (vehicle_type) => {
    return build_validation_result(true, '', '')
};

const validate_document_type = (document_type) => {
    return build_validation_result(true, '', '')
};

const validate_line_coverage = (policy_type, coverage_type) => {
    const policy_types = ['commercial auto', 'auto', 'business auto', 'auto application', 'auto app'];
    if (policy_type && !policy_types.includes(policy_type.toString().toLowerCase())) {
        return build_validation_result(false,
            'policyType',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.');
    } else {
        return build_validation_result(true, '', '')
    }
}

const validate_vehicle_coverage = (coverage_type) => {
    return build_validation_result(true, '', '')
};


// ----------------------------------------- Functions that control the bot's behavior --------------------------------------- //


async function submission_instruction(intent_request) {
    // Performs validations on policy type slot and then returns new submission instructions as needed.

    const policy_type = get_slots(intent_request)["policyType"]
    // Perform basic validation on the supplied input slots.
    const validation_result = validate_policy_type(policy_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return delegate('Fulfilled', errorMessage);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
}

async function add_vehicle(intent_request) {

    // Provides instructions on adding a new vehicle to an auto policy.

    const vehicle_type = get_slots(intent_request)["vehicleType"]

    // Perform basic validation on the supplied input slots.
    const validation_result = validate_vehicle_type(vehicle_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        return delegate('Fulfilled', errorMessage);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('vehicle_type', vehicle_type), response["RelatedQuestions"])
    }
}

async function generate_document(intent_request) {

    // Provides instructions on how to generate a new document.


    const document_type = get_slots(intent_request)["documentType"]

    //Perform basic validation on the supplied input slots.
    const validation_result = validate_document_type(document_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        return delegate('Fulfilled', errorMessage)
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('document_type', document_type), response["RelatedQuestions"])
    }
}

async function line_level_coverage(intent_request) {

    //Provides instructions on updating line level coverages.


    const coverage_type = get_slots(intent_request)["coverageType"]
    const policy_type = get_slots(intent_request)["policyType"]
    const state_coverages = ['uninsured motor vehicle', 'underinsured motor vehicle', 'state info']
    let screen_name = ['Commercial Auto Line']

    if (state_coverages.includes(coverage_type.toLowerCase())) {
        screen_name = 'State Info'

    }
    //(any(coverage_type.lower() in state_cov for state_cov in state_coverages)) 


    //Perform basic validation on the supplied input slots.
    const validation_result = validate_line_coverage(policy_type, coverage_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        if (validation_result['violatedSlot'] == 'policyType') {
            const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
            return delegate('Fulfilled', errorMessage)
        }
    }
    else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('coverage_type', coverage_type).replace('screen_name', screen_name), response["RelatedQuestions"])
    }
}

async function vehicle_level_coverage(intent_request) {

    //Provides instructions on updating vehicle level coverages.

    const coverage_type = get_slots(intent_request)["vehicleCoverage"]
    //Perform basic validation on the supplied input slots.
    const validation_result = validate_vehicle_coverage(coverage_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        if (validation_result['violatedSlot'] == 'vehicleCoverage') {
            const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
            return delegate('Fulfilled', errorMessage)
        }

    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return delegate('Fulfilled', response["Response"].replace('coverage_type', coverage_type), response["RelatedQuestions"])
    }
}

// ------------------------------------------ Dispatch Intents ---------------------------------------------- //

async function dispatch(intent_request) {
    //Called when the user specifies an intent for this bot.
    const intent_name = intent_request['currentIntent']['name']
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'submission_instruction') {
        return await submission_instruction(intent_request);
    }
    else if (intent_name == 'add_vehicle') {
        return await add_vehicle(intent_request);
    } else if (intent_name == 'generate_document') {
        return await generate_document(intent_request);
    } else if (intent_name == 'line_level_coverage') {
        return await line_level_coverage(intent_request);
    } else if (intent_name == 'vehicle_level_coverage') {
        return await vehicle_level_coverage(intent_request);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return await delegate('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
};

// ---------------------------------------------------- Main handler ------------------------------------------------- //

exports.handler = async function (event, context) {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
};
