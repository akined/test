'use strict'
const AWS = require('aws-sdk');

// ---------------------------------------------------- DynamoDb connection set up. ------------------------------------------ //
const dynamodb = new AWS.DynamoDB({});
const dynamoTable = 'sf-conncen-test-podd-pcassistantbot_DB';

const descriptors = ['L', 'M', 'N', 'S', 'B', 'SS', 'NULL', 'NS', 'BS'];
const flattenDynamoDbResponse = (o) => {

    // flattens single property objects that have descriptors  
    for (let d of descriptors) {
        if (o.hasOwnProperty(d)) {
            return o[d];
        }
    }

    Object.keys(o).forEach((k) => {

        for (let d of descriptors) {
            if (o[k].hasOwnProperty(d)) {
                o[k] = o[k][d];
            }
        }
        if (Array.isArray(o[k])) {
            o[k] = o[k].map(e => flattenDynamoDbResponse(e))
        } else if (typeof o[k] === 'object') {
            o[k] = flattenDynamoDbResponse(o[k])
        }
    });

    return o;
}

async function get_response(intent_name) {
    try {
        // Get the FAQ respnose for given intent_name.

        const intentResponse = {}
        const params = {
            Key: {
                'IntentName': {
                    S: intent_name
                }
            },
            TableName: dynamoTable
        };
        console.log(intentResponse, "intentresponse ")
        const dynamodbResponse = await dynamodb.getItem(params).promise();
        console.log(dynamodbResponse, "dyanmoresponse ")
        const response = flattenDynamoDbResponse(dynamodbResponse);
        console.log(response, "response ")
        intentResponse["Response"] = response['Item']['FAQResponse'];

        //intentResponse["RelatedQuestions"] = response['Item']['RelatedQuestions'];

        //console.log("The related questions are as follows: ", intentResponse["RelatedQuestions"]);
        console.log("The intent resopnse is :", intentResponse);

        // Finally return the composed intent response to elicitslot.
        return intentResponse;
    } catch (err) {
        console.log("Error occured while trying to retrieve response for intent: ", intent_name);
        return err.response['Error']['Message'];
    }
}

// ---------------- Helpers to build responses which match the structure of the necessary dialog actions ----------------------- //

const get_slots = (intent_request) => intent_request['currentIntent']['slots'];

const elicitSlotForTakePayment = (fulfillment_state, message, related_intents = [], intent_request = {}) => {
    const dialogResponse = {
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            //   'intentName': intent_request['currentIntent']['name'],
            'message': {
                'contentType': 'CustomPayload',
                'content': "Custom Message"
            },
            // 'slots': intent_request['currentIntent']['slots'],
            //  'slotToElicit':Object.keys(intent_request['currentIntent']['slots'])[0],
        }
    }
    // Check if we are given a list of related questions for this intent.
    // if (related_intents.length > 0) {
    // Generate the response card to be added to dialogResponse.
    const responseCard = {
        'version': 1,
        'contentType': 'application/vnd.amazonaws.card.generic',
        'genericAttachments': []
    }
    const questions = [
        { text: 'Take Payment', value: 'Take Payment' },
        { text: 'Policyholder Owes', value: 'Policyholder Owes' },
        { text: 'Billing Answers', value: 'Billing Answers' },
    ];
    console.log("Question buttons are ", questions);
    // Create the generic attachment to add to the lex response.
    const attachment = {
        'title': 'You can also ask..',
        'buttons': questions
    };
    console.log("The generic attachment is : ", attachment);
    // Attach the attachments to the response card.
    responseCard['genericAttachments'].push(attachment)
    // Attach response card to dialogResponse.
    dialogResponse['dialogAction'] = responseCard
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    // }
    return dialogResponse;
}

const elicitSlot = (fulfillment_state, message, related_intents = [], intent_request = {}) => ({
    'dialogAction': {
        'type': 'Close',
        'fulfillmentState': fulfillment_state,
        //   'intentName': intent_request['currentIntent']['name'],
        'message': {
            'contentType': 'CustomPayload',
            'content': message
        },
        // 'slots': intent_request['currentIntent']['slots'],
        //  'slotToElicit':Object.keys(intent_request['currentIntent']['slots'])[0],
    }
});

const elicitSlot_NoSlot = (fulfillment_state, message, related_intents = [], intent_request = {}) => {
    const dialogResponse = {
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': 'Fulfilled',
            //'intentName': intent_request['currentIntent']['name'],
            'message': {
                'contentType': 'CustomPayload',
                'content': message
            },
            //'slots': intent_request['currentIntent']['slots'],
            //'slotToElicit':Object.keys(intent_request['currentIntent']['slots'])[0],
        }
    }
    // Check if we are given a list of related questions for this intent.
    /*   if (related_intents.length > 0) {
           // Generate the response card to be added to dialogResponse.
           const responseCard = {
               'version': 1,
               'contentType': 'application/vnd.amazonaws.card.generic',
               'genericAttachments': []
           }
           const questions = []
           // Create buttons for each of the realted intent question.
           related_intents.map((question) => {
               const question_button = {
                   'text': question,
                   'value': question
               };
               questions.push(question_button);
           });
   
           console.log("Question buttons are ", questions);
           // Create the generic attachment to add to the lex response.
           const attachment = {
               'title': 'You can also ask..',
               'buttons': questions
           };
           console.log("The generic attachment is : ", attachment);
           // Attach the attachments to the response card.
           responseCard['genericAttachments'].push(attachment)
           // Attach response card to dialogResponse.
           dialogResponse['dialogAction']['responseCard'] = responseCard
           console.log("The dialogResponse will be formatted as : ", dialogResponse);
       }*/
    return dialogResponse
}


// ---------------------------------------------- Validation Helper Functions ---------------------------------------------- //

const build_validation_result = (is_valid, violated_slot, message_content) => {
    if (message_content) {
        return {
            "isValid": is_valid,
            "violatedSlot": violated_slot,
        }
    } else {
        return {
            'isValid': is_valid,
            'violatedSlot': violated_slot,
            'message': { 'contentType': 'CustomPayload', 'content': message_content }
        }
    }
};
const validate_vehicle_type = (vehicle_type) => {
    return build_validation_result(true, '', '')
};

const validate_document_type = (document_type) => {
    const document_types = ['selection rejection', 'sel/rej', 'sel rej', 'selrej', 'um form', 'uninsured form', 'u form',
        'u-form', 'underinsured form', 'uninsured motorist form', 'uninsures mostorist form', 'selection / rejection forms', 'uform', 'select reject', 'photos', 'photo'];
    console.log(document_type, "document_type")
    if (document_type && !document_types.includes(document_type.toString().toLowerCase())) {
        return build_validation_result(false,
            'document',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.')
    } else {
        return build_validation_result(true, '', '');
    }
};

const validate_activity = (activity) => {
    const activities = ['upload', 'attach', 'migrate', 'signed document'];
    if (activity && !activities.includes(activity.toString().toLowerCase())) {
        return build_validation_result(false,
            'ActivityIntent',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.');
    } else {
        return build_validation_result(true, '', '')
    }
};

// ----------------------------------------- Functions that control the bot's behavior --------------------------------------- //

async function take_payment(intent_request) {
    const response = await get_response(intent_request['currentIntent']['name']);
    return elicitSlotForTakePayment('Fulfilled', response["Response"], response["RelatedQuestions"], intent_request)
}
async function policyholder_owes(intent_request) {
    const response = await get_response(intent_request['currentIntent']['name'])

    return elicitSlot_NoSlot('Fulfilled', response["Response"], response["RelatedQuestions"], intent_request)
}
async function careform_in_abs(intent_request) {
    const response = await get_response(intent_request['currentIntent']['name'])

    return elicitSlot_NoSlot('Fulfilled', response["Response"], response["RelatedQuestions"], intent_request)
}
async function billing_answers(intent_request) {

    const response = await get_response(intent_request['currentIntent']['name'])
    console.log({ response })
    return elicitSlot_NoSlot('Fulfilled', response["Response"], response["RelatedQuestions"], intent_request)

}
async function policy_status(intent_request) {
    const response = await get_response(intent_request['currentIntent']['name'])
    return elicitSlot_NoSlot('Fulfilled', response["Response"], response["RelatedQuestions"], intent_request)
}
async function extend_workspace(intent_request) {
    const response = await get_response(intent_request['currentIntent']['name'])
    return elicitSlot_NoSlot('Fulfilled', response["Response"], response["RelatedQuestions"], intent_request)

}
async function change_quote(intent_request) {
    const response = await get_response(intent_request['currentIntent']['name'])
    return elicitSlot_NoSlot('Fulfilled', response["Response"], response["RelatedQuestions"], intent_request)

}
async function Commercial_autoid(intent_request) {
    const response = await get_response(intent_request['currentIntent']['name'])
    return elicitSlot_NoSlot('Fulfilled', response["Response"], response["RelatedQuestions"], intent_request)

}
async function complete_activity(intent_request) {
    const activity = get_slots(intent_request)["ActivityIntent"]
    const validation_result = validate_activity(activity)
    if (!validation_result['isValid']) {
        const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        return elicitSlot('Fulfilled', errorMessage, [], intent_request);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return elicitSlot('Fulfilled', response["Response"].replace('activity', activity), response["RelatedQuestions"], intent_request)
    }
}
async function upload_documents(intent_request) {
    const document_type = get_slots(intent_request)["document"]
    const validation_result = validate_document_type(document_type)
    console.log(validation_result, "validation result")
    if (!validation_result['isValid']) {
        const errorMessage = 'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.'
        console.log("insideif")
        return elicitSlot('Fulfilled', errorMessage, [], intent_request);

    } else {
        console.log("else")
        const response = await get_response(intent_request['currentIntent']['name'])
        return elicitSlot('Fulfilled', response["Response"].replace('document_type', document_type), response["RelatedQuestions"], intent_request)
    }
}
async function add_vehicle(intent_request) {

    // Provides instructions on adding a new vehicle to an auto policy.

    const vehicle_type = get_slots(intent_request)["vehicleType"]

    // Perform basic validation on the supplied input slots.
    const validation_result = validate_vehicle_type(vehicle_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        return elicitSlot('Fulfilled', errorMessage, [], intent_request);
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return elicitSlot('Fulfilled', response["Response"].replace('vehicle_type', vehicle_type), response["RelatedQuestions"], intent_request)
    }
}

async function generate_document(intent_request) {

    // Provides instructions on how to generate a new document.
    const document_type = get_slots(intent_request)["document"]

    //Perform basic validation on the supplied input slots.
    const validation_result = validate_document_type(document_type)
    if (!validation_result['isValid']) {
        const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
        return elicitSlot('Fulfilled', errorMessage, [], intent_request)
    } else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return elicitSlot('Fulfilled', response["Response"].replace('document_type', document_type), response["RelatedQuestions"], intent_request)
    }
}


// ------------------------------------------ Dispatch Intents ---------------------------------------------- //

async function dispatch(intent_request) {
    //Called when the user specifies an intent for this bot.
    console.log(intent_request)

    const intent_name = intent_request['currentIntent']['name']
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'add_vehicle') {
        return await add_vehicle(intent_request);
    } else if (intent_name == 'generate_document') {
        return await generate_document(intent_request);
    } else if (intent_name == 'take_payment') {
        return await take_payment(intent_request)
    } else if (intent_name == 'careform_in_abs') {
        return await careform_in_abs(intent_request)
    } else if (intent_name == 'complete_activity') {
        return await complete_activity(intent_request)
    } else if (intent_name == 'billing_answers') {
        return await billing_answers(intent_request)
    } else if (intent_name == 'extend_workspace') {
        return await extend_workspace(intent_request)
    } else if (intent_name == 'upload_documents') {
        return await upload_documents(intent_request)
    } else if (intent_name == 'policy_status') {
        return await policy_status(intent_request)
    } else if (intent_name == 'change_quote') {
        return await change_quote(intent_request)
    } else if (intent_name == 'Commercial_autoid') {
        return await Commercial_autoid(intent_request)
    } else if (intent_name == 'policyholder_owes') {
        return await policyholder_owes(intent_request)
    }
    else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return await elicitSlot('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
};

// ---------------------------------------------------- Main handler ------------------------------------------------- //

exports.handler = async function (event, context) {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
}
