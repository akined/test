'use strict'
const AWS = require('aws-sdk');

// ---------------------------------------------------- DynamoDb connection set up. ------------------------------------------ //
const dynamodb = new AWS.DynamoDB({});
const ses = new AWS.SES({});
const dynamoTable = '';

const descriptors = ['L', 'M', 'N', 'S', 'B', 'SS', 'NULL', 'NS', 'BS'];
const flattenDynamoDbResponse = (o) => {

    // flattens single property objects that have descriptors  
    for (let d of descriptors) {
        if (o.hasOwnProperty(d)) {
            return o[d];
        }
    }

    Object.keys(o).forEach((k) => {

        for (let d of descriptors) {
            if (o[k].hasOwnProperty(d)) {
                o[k] = o[k][d];
            }
        }
        if (Array.isArray(o[k])) {
            o[k] = o[k].map(e => flattenDynamoDbResponse(e))
        } else if (typeof o[k] === 'object') {
            o[k] = flattenDynamoDbResponse(o[k])
        }
    });

    return o;
}

async function get_response(alias) {
    try {
        const params = {
            Key: {
                'Alias': {
                    S: alias
                }
            },
            TableName: dynamoTable
        };
        const dynamodbResponse = await dynamodb.getItem(params).promise();
        const response = flattenDynamoDbResponse(dynamodbResponse);
        console.log("The alias resopnse is :", response);
        return response;
    } catch (err) {
        console.log("Error occured while trying to retrieve response for alias: ", alias);
        return err.response['Error']['Message'];
    }
}

async function sendEmailViaSes() {
    const params = {
        Source: "sender@example.com",
        Destination: {
            ToAddresses: [
                "recipient1@example.com",
            ]
        },
        Message: {
            Body: {
                Html: {
                    Charset: "UTF-8",
                    Data: "This message body contains HTML formatting. It can, for example, contain links like this one: <a class=\"ulink\" href=\"http://docs.aws.amazon.com/ses/latest/DeveloperGuide\" target=\"_blank\">Amazon SES Developer Guide</a>."
                },
                Text: {
                    Charset: "UTF-8",
                    Data: "This is the message body in text format."
                }
            },
            Subject: {
                Charset: "UTF-8",
                Data: "Test email"
            }
        },
        ReplyToAddresses: [
        ],
    };
    const sesResponse = await ses.sendEmail(params).promise();
    return sesResponse;
}

exports.handler = async function (event, context) {
    try {
        const alias = intent_request['currentIntent']['slots']['alias'];
        const userData = await get_response(alias);
        const emailResponse = await sendEmailViaSes(userData);
        return emailResponse;
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
};
