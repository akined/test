'use strict';

const AWS = require('aws-sdk');

const close = (fulfillment_state, message, session_attributes = {}) => {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message
        }
    };
    console.log("===========close dialoge response============", dialogResponse);
    return dialogResponse;
};

const elicitSlot = (sessionAttributes, intentName, slots, slotToElicit, message) => {
    const dialogResponse = {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,
        },
    };
    console.log("===========elicitSlot dialoge response============", dialogResponse);
    return dialogResponse;
};

// ------------------------------------------ Dispatch Intents ---------------------------------------------- //

async function billingIntent(intent_request) {
    const inputFromUser = intent_request["inputTranscript"];
    const session_attributes = {
        'HDR_5_ReasonForCall': inputFromUser,
    };
    intent_request["sessionAttributes"] = session_attributes;
    console.log("====billingIntent======", session_attributes);
    const message = {
        'contentType': 'SSML',
        'content': `<speak>The input is  you said is ${inputFromUser}, Is this correct?</speak>`
    };
    return close('Fulfilled', message, session_attributes);
}

async function fallbackIntent(intent_request) {
    const inputFromUser = intent_request["inputTranscript"];
    const session_attributes = {
        'HDR_5_ReasonForCall': inputFromUser,
    };
    intent_request["sessionAttributes"] = session_attributes;
    console.log("====fallbackIntent======", session_attributes);
    const message = {
        'contentType': 'SSML',
        'content': `<speak>The input is  you said is ${inputFromUser}, Is this correct?</speak>`
    };
    return close('Fulfilled', message, session_attributes);
}

async function dispatch(intent_request) {
    console.log("====intent_request======", JSON.stringify(intent_request));

    const intent_name = intent_request['currentIntent']['name'];
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'PCU_BLRC_PC_Billing_Intent') {
        return await billingIntent(intent_request);
    }
    else if (intent_name == 'PCU_BLRC_PC_Fallback_Intent') {
        return await fallbackIntent(intent_request);
    }
}

exports.handler = async (event) => {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
};
