'use strict'
const AWS = require('aws-sdk');

// ---------------------------------------------------- DynamoDb connection set up. ------------------------------------------ //
const dynamodb = new AWS.DynamoDB({});
const dynamoTable = 'sf-conccen-rsch-podd-us1-claimdetails';

const descriptors = ['L', 'M', 'N', 'S', 'B', 'SS', 'NULL', 'NS', 'BS'];
const flattenDynamoDbResponse = (o) => {

    // flattens single property objects that have descriptors  
    for (let d of descriptors) {
        if (o.hasOwnProperty(d)) {
            return o[d];
        }
    }

    Object.keys(o).forEach((k) => {

        for (let d of descriptors) {
            if (o[k].hasOwnProperty(d)) {
                o[k] = o[k][d];
            }
        }
        if (Array.isArray(o[k])) {
            o[k] = o[k].map(e => flattenDynamoDbResponse(e))
        } else if (typeof o[k] === 'object') {
            o[k] = flattenDynamoDbResponse(o[k])
        }
    });

    return o;
}

async function get_response(intent_name) {
    try {
        // Get the FAQ respnose for given intent_name.

        const intentResponse = {}
        const params = {
            Key: {
                'CustomerPhoneNumber': {
                    S: intent_name
                }
            },
            TableName: dynamoTable
        };

        console.log(intentResponse, "intentresponse ")
        const dynamodbResponse = await dynamodb.getItem(params).promise();
        console.log(dynamodbResponse, "dyanmoresponse ")
        const response = flattenDynamoDbResponse(dynamodbResponse);
        console.log(response, "response ")
        intentResponse["Response"] = response['Item'];


        console.log("The intent resopnse is :", intentResponse);

        // Finally return the composed intent response to elicitslot.
        return intentResponse;
    } catch (err) {
        console.log("Error occured while trying to retrieve response for intent: ", intent_name);
        return err.response['Error']['Message'];
    }
}

// ---------------- Helpers to build responses which match the structure of the necessary dialog actions ----------------------- //

const get_slots = (intent_request) => intent_request['currentIntent']['slots'];

const confirmIntent = (fulfillment_state, message, related_intents = [], intent_request = {}) => {
    console.log("===========================", intent_request);
    const dialogResponse = {
        sessionAttributes: {},
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intent_request['currentIntent']['name'],
            'message': message,
            slots: {
                numbers: intent_request['inputTranscript']
            }
        }
    }
    return dialogResponse;
}
const delegate = (fulfillment_state, message, related_intents = [], intent_request = {}) => {
    console.log("================message===========", message);
    const dialogResponse = {
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': {
                'contentType': 'CustomPayload',
                'content': message['DateOfLoss']
            }
        }
    };
    console.log("===============dialog resoone============", dialogResponse);
    return dialogResponse;
}



// ---------------------------------------------- Validation Helper Functions ---------------------------------------------- //

const build_validation_result = (is_valid, violated_slot, message_content) => {
    if (message_content) {
        return {
            "isValid": is_valid,
            "violatedSlot": violated_slot,
        }
    } else {
        return {
            'isValid': is_valid,
            'violatedSlot': violated_slot,
            'message': { 'contentType': 'CustomPayload', 'content': message_content }
        }
    }
};

const validate_document_type = (document_type) => {
    const document_types = ['selection rejection', 'sel/rej', 'sel rej', 'selerej', 'um form', 'uninsured form', 'u form',
        'u-form', 'underinsured form', 'uninsured motorist form', 'uninsures mostorist form', 'selection / rejection forms', 'uform', 'select reject', 'photos', 'photo'];
    console.log(document_type, "document_type")
    if (document_type && !document_types.includes(document_type.toString().toLowerCase())) {
        return build_validation_result(false,
            'document',
            'I can only assist with questions on Business Auto policy at this time. For further assistance, please contact your normal support channel.')
    } else {
        return build_validation_result(true, '', '');
    }
};

// ----------------------------------------- Functions that control the bot's behavior --------------------------------------- //


async function number(intent_request) {
    const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];
    if (confirmationStatus == 'None') {
        const message = {
            'contentType': 'PlainText',
            'content': `Is this the number ${intent_request["inputTranscript"]} you are saying`
        };
        return confirmIntent('Fulfilled', message, [], intent_request);


    } else {
        const response = await get_response(intent_request['currentIntent']['slots']['numbers']);
        return delegate('Fulfilled', response["Response"]);
    }


    // Provides instructions on how to generate a new document.
    // const document_type = get_slots(intent_request)["forms"]

    // //Perform basic validation on the supplied input slots.
    // const validation_result = validate_document_type(document_type)
    // if (!validation_result['isValid']) {
    //     const errorMessage = 'Sorry, I am unable to assist you with that problem at the moment. Please contact customer support channel.'
    //     return elicitSlot('Fulfilled', errorMessage, [], intent_request)
    // } else {
    //     const response = await get_response(intent_request['currentIntent']['name'])
}
// ------------------------------------------ Dispatch Intents ---------------------------------------------- //

async function dispatch(intent_request) {
    //Called when the user specifies an intent for this bot.
    console.log(intent_request)

    const intent_name = intent_request['currentIntent']['name']
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'Number') {
        return await number(intent_request);
    }
    else {
        const response = await get_response(intent_request['currentIntent']['name'])
        return await confirmIntent('Fulfilled', response["Response"], response["RelatedQuestions"])
    }
};

// ---------------------------------------------------- Main handler ------------------------------------------------- //

exports.handler = async function (event, context) {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
}
