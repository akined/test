'use strict';

const AWS = require('aws-sdk');
const lambda = new AWS.Lambda({});
process.env.AWS_NODEJS_CONNECTION_REUSE_ENABLED = 1;
const customerlambda = process.env.CUSTOMER_LAMBDA;
const aggrementLambda = process.env.AGREEMENTINDEX_LAMBDA;
const dvlLambda = process.env.DVL_LAMBDA;

async function confimIntent(session_attributes, intentName, message) {
    const dialogResponse = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intentName,
            'message': {
                'contentType': 'CustomPayload',
                'content': message
            },
            'responseCard': {
                'version': 1,
                'contentType': 'application/vnd.amazonaws.card.generic',
                'genericAttachments': [{
                    'buttons': [
                        { text: 'Yes', value: 'Yes' },
                        { text: 'No', value: 'No' },
                    ]
                }]
            }
        }
    };
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}
const close = (fulfillment_state, message, session_attributes = {}) => {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': {
                'contentType': 'PlainText',
                'content': message
            }
        }
    };
    console.log("===========close dialoge response============", dialogResponse);
    return dialogResponse;
};

const elicitIntent = (sessionAttributes, message) => {
    const dialogResponse = {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitIntent',
            message
        }
    };
    console.log("=====elicitIntent dialog Response=====", dialogResponse);
    return dialogResponse;

};

const elicitSlot = (sessionAttributes, intentName, slots, slotToElicit, message) => {
    const dialogResponse = {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,
        },
    };
    console.log("===========elicitSlot dialoge response============", dialogResponse);
    return dialogResponse;
};

async function buttonsforautofire(finalCustomeroutput, slotsData) {
    const dialogResponse = {
        'sessionAttributes': {
            customerOutput: JSON.stringify(finalCustomeroutput),
            slots: JSON.stringify(slotsData)
        },
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': {
                'contentType': 'CustomPayload',
                'content': "Is your Inquiry about Auto or Fire related?"
            },

        }
    };
    const responseCard = {
        'version': 1,
        'contentType': 'application/vnd.amazonaws.card.generic',
        'genericAttachments': []
    };
    const questions = [
        { text: 'Auto', value: 'Auto' },
        { text: 'Fire', value: 'Fire' }
    ];
    console.log("Question buttons are ", questions);
    // Create the generic attachment to add to the lex response.
    const attachment = {
        'buttons': questions
    };
    console.log("The generic attachment is : ", attachment);

    responseCard['genericAttachments'].push(attachment);
    dialogResponse['dialogAction']['responseCard'] = responseCard;
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}
async function buttonsForMakeModelYear(intent_request, makeModeYearData, agreementPayloadData) {
    const dialogResponse = {
        'sessionAttributes': {
            ...intent_request["sessionAttributes"],
            makeModelYearData: JSON.stringify(makeModeYearData),
            agreementData: JSON.stringify(agreementPayloadData)
        },
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': {
                'contentType': 'CustomPayload',
                'content': "Please Click on the make model and year of the vehicle you want to place the order for"
            },

        }
    };

    const responseCard = {
        'version': 1,
        'contentType': 'application/vnd.amazonaws.card.generic',
        'genericAttachments': []
    };

    const buttons = makeModeYearData.map((data) => ({
        text: data,
        value: `This is the Year Make Model selected,${data}`,
    }));

    const totalFourQuestions = buttons.slice(0, 4);
    console.log("totalFourQuestions buttons are ", totalFourQuestions);

    totalFourQuestions.push(
        {
            text: 'Not listed',
            value: 'NotListed'
        });

    console.log("Question buttons are ", totalFourQuestions);
    // Create the generic attachment to add to the lex response.
    const attachment = {
        'buttons': totalFourQuestions
    };
    console.log("The generic attachment is : ", attachment);

    responseCard['genericAttachments'].push(attachment);
    dialogResponse['dialogAction']['responseCard'] = responseCard;
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}

async function buttonsForPolicies(intent_request, policies, agreementPayloadData) {
    const dialogResponse = {
        'sessionAttributes': {
            ...intent_request["sessionAttributes"],
            policies: JSON.stringify(policies),
            agreementData: JSON.stringify(agreementPayloadData)
        },
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': {
                'contentType': 'CustomPayload',
                'content': "Please Click on the policy you are looking for"
            },

        }
    };

    const responseCard = {
        'version': 1,
        'contentType': 'application/vnd.amazonaws.card.generic',
        'genericAttachments': []
    };

    const buttons = policies.map((data) => ({
        text: data,
        value: `This is the Policy Number selected,${data}`,
    }));

    const totalFourQuestions = buttons.slice(0, 4);
    console.log("totalFourQuestions buttons are ", totalFourQuestions);

    totalFourQuestions.push(
        {
            text: 'Not listed',
            value: 'NotListed'
        });

    console.log("Question buttons are ", totalFourQuestions);
    // Create the generic attachment to add to the lex response.
    const attachment = {
        'buttons': totalFourQuestions
    };
    console.log("The generic attachment is : ", attachment);

    responseCard['genericAttachments'].push(attachment);
    dialogResponse['dialogAction']['responseCard'] = responseCard;
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
}

async function invokeLambda(lambdaName, inputData) {
    try {
        const params = {
            FunctionName: lambdaName,
            Payload: JSON.stringify(inputData)
        };
        console.log("=========params=========", JSON.stringify(params));
        const output = await lambda.invoke(params).promise();
        console.log("=======invokeLambda========", JSON.stringify(output));
        return output;
    } catch (err) {
        console.log("Error While Executing Step Function", err);
        return err;
    }
}

async function customerlambdaOutput(intent_request) {
    const slots = intent_request['currentIntent']['slots'];
    const message = `Thank you, I am connecting you to a representative for your Inquiry \n
                    Phone Number: ${slots["phoneNumber"]},\n
                    Birth Date: ${slots["dateOfbirth"]}`;
    const lambdaOutput = await invokeLambda(
        customerlambda,
        { birthDate: slots.dateOfbirth, phone: slots.phoneNumber }
    );
    console.log("lambdaOutput", lambdaOutput);
    if (lambdaOutput['StatusCode'] == 200) {
        const finalCustomeroutput = JSON.parse(lambdaOutput.Payload);
        const finalData = typeof finalCustomeroutput == "string" ? close('Fulfilled', message) :
            await buttonsforautofire(finalCustomeroutput, slots);
        return finalData;
    }
    else {
        return close('Fulfilled', message);
    }
}

async function customerData(intent_request) {
    const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    const slots = intent_request['currentIntent']['slots'];
    const phonenumber = intent_request['currentIntent']['slots']['phoneNumber'];
    const dateofbirth = intent_request['currentIntent']['slots']['dateOfbirth'];
    console.log("outside  part", intent_request['currentIntent']['slots']);
    if (phonenumber != null && dateofbirth != null) {
        console.log("inside the phonenumber and dob is not null part");
        const filteredNumber = phonenumber.replace(/[^\d]/g, '');
        console.log("filteredNumber", filteredNumber);
        if (filteredNumber.length == 10) {
            console.log("=======filteredNumber length is 10==========", filteredNumber);
            intent_request['currentIntent']['slots']['phoneNumber'] = filteredNumber;
        } else {
            console.log("=======filteredNumber length is > 10==========", filteredNumber);
            const finalNumber = filteredNumber.substr(1);
            intent_request['currentIntent']['slots']['phoneNumber'] = finalNumber;
        }
        console.log("phonenumber", intent_request['currentIntent']['slots']['phoneNumber']);
        return await customerlambdaOutput(intent_request);
    } else if (phonenumber != null) {
        console.log("inside the phonenumber is not null part", intent_request['currentIntent']['slots']);
        return elicitSlot(session_attributes, intentName, slots, 'dateOfbirth');
    } else if (phonenumber == null) {
        console.log("inside the phonenumber null part", intent_request['currentIntent']['slots']);
        return elicitSlot(session_attributes, intentName, slots, 'phoneNumber');
    }
}



async function autofireData(intent_request, prodLine) {
    // const intentName = intent_request['currentIntent']['name'];
    const session_attributes = intent_request["sessionAttributes"];
    console.log("outside  part", intent_request['currentIntent']['slots']);
    const slots = JSON.parse(session_attributes['slots']);
    const customerOutput = JSON.parse(session_attributes['customerOutput']);
    console.log("customer output", customerOutput);
    if (intent_request['currentIntent']['slotDetails']['autoFire']['resolutions'][0]['value'] == ['Auto']) {

        const lambdaOutput = await invokeLambda(
            aggrementLambda,
            { customerId: customerOutput.customerId, prodLine: "A" }
        );
        console.log("prodline");
        const payloadres = JSON.parse(lambdaOutput.Payload);
        console.log("======autoData =====lambdaOutput ===", JSON.stringify(payloadres));
        const makeModelYearData = payloadres.map((data) => data.risks.riskDescTxt);
        console.log("======autoData =====makeModelYearData ===", JSON.stringify(makeModelYearData));
        return await buttonsForMakeModelYear(intent_request, makeModelYearData, payloadres);
    } else {
        const lambdaOutput = await invokeLambda(
            aggrementLambda,
            { customerId: customerOutput.customerId, prodLine: "F" }
        );
        const payloadres = JSON.parse(lambdaOutput.Payload);
        console.log("======autoData =====lambdaOutput ===", JSON.stringify(payloadres));
        const policies = payloadres.map((data) => data.agreement.agreDsplyNum);
        console.log("======autoData =====makeModelYearData ===", JSON.stringify(policies));
        return await buttonsForPolicies(intent_request, policies, payloadres);
    }

}

async function yeakMakeModel(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const finalAttributes = { ...session_attributes, makeModelInputTranscript: intent_request['inputTranscript'] };
    const message = {
        'contentType': 'CustomPayload',
        'content': "Please state the coverage or discount you have a question about."
    };
    return elicitIntent(finalAttributes, message);
}
async function policyNumber(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const finalAttributes = { ...session_attributes, policies: intent_request['inputTranscript'] };
    const message = {
        'contentType': 'CustomPayload',
        'content': "Please state the coverage or discount you have a question about."
    };
    console.log("====finalAttributes=============", JSON.stringify(finalAttributes));
    return elicitIntent(finalAttributes, message);
}


async function autoDiscountCoverage(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const intentName = intent_request['currentIntent']['name'];
    const discountSlotDetails = intent_request["currentIntent"]["slotDetails"]["discount"];
    const inputValue = discountSlotDetails["resolutions"].length > 0 ? discountSlotDetails["resolutions"][0]["value"] :
        discountSlotDetails["originalValue"];
    const agreementDataSessionAttribute = JSON.parse(session_attributes["agreementData"]);
    console.log("-===makeModelInputTranscript====key invokde========");
    const splittedData = session_attributes["makeModelInputTranscript"].split(",");
    console.log("-===makeModelInputTranscript====key splittedData========", splittedData);
    const finalData = agreementDataSessionAttribute.find(item => item.risks.riskDescTxt == splittedData[1]);
    console.log("-===discountCoverage====finalData========", JSON.stringify(finalData));
    const lambdaOutput = await invokeLambda(
        dvlLambda,
        { agreAccessKey: finalData.agreement.agreAccessKey, agreSrcSysCd: finalData.agreement.agreSrcSysCd }
    );
    const payloadres = JSON.parse(lambdaOutput.Payload);
    console.log("======discountCoverage =====lambdaOutput ===", JSON.stringify(payloadres));
    if (payloadres["policy"]["termVersion"]["insurableRisk"][0]["pricingRuleSet"]) {
        const pricingData = payloadres["policy"]["termVersion"]["insurableRisk"][0]["pricingRuleSet"]["pricingAdjustment"];
        console.log("=====pricingData=====", JSON.stringify(pricingData));
        const finalAmount = pricingData.find((item) => item.pricingRuleSetDisplayName.toLowerCase() == inputValue.toLowerCase());
        console.log("=====finalAmount=====", JSON.stringify(finalAmount));
        if (finalAmount) {
            const message = `The customer has Comprehensive Coverage in the amount of ${finalAmount.pricingRuleSetAdjustmentAmount}, does this answer your question?`;
            return await confimIntent(session_attributes, intentName, message);
        }
        else {
            const message = "The customer does not have the discount or coverage you asked about on the policy selected. Do you need anything else or does that answer your question?";
            return await confimIntent(session_attributes, intentName, message);
        }
    } else {
        const message = "The customer does not have the discount or coverage you asked about on the policy selected. Do you need anything else or does that answer your question?";
        return await confimIntent(session_attributes, intentName, message);
    }
}

async function fireDiscountCoverage(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const intentName = intent_request['currentIntent']['name'];
    const discountSlotDetails = intent_request["currentIntent"]["slotDetails"]["discount"];
    const inputValue = discountSlotDetails["resolutions"].length > 0 ? discountSlotDetails["resolutions"][0]["value"] :
        discountSlotDetails["originalValue"];
    const agreementDataSessionAttribute = JSON.parse(session_attributes["agreementData"]);
    console.log("-===fire part invoked====finalData========");
    const splittedData = session_attributes["policies"].split(",");
    console.log("-===fire part invoked====splittedData========", splittedData);
    const finalData = agreementDataSessionAttribute.find(item => item.agreement.agreDsplyNum == splittedData[1]);
    const lambdaOutput = await invokeLambda(
        dvlLambda,
        { agreAccessKey: finalData.agreement.agreAccessKey, agreSrcSysCd: finalData.agreement.agreSrcSysCd }
    );
    const payloadres = JSON.parse(lambdaOutput.Payload);
    console.log("======discountCoverage =====lambdaOutput ===", JSON.stringify(payloadres));
    if (payloadres["policy"]["termVersion"]["policyPricingRuleSet"]) {
        const pricingData = payloadres["policy"]["termVersion"]["policyPricingRuleSet"]["pricingAdjustment"];
        console.log("=====pricingData=====", JSON.stringify(pricingData));
        const finalAmount = pricingData.find((item) => item.pricingRuleSetDisplayName.toLowerCase() == inputValue.toLowerCase());
        console.log("=====finalAmount=====", JSON.stringify(finalAmount));
        if (finalAmount) {
            const message = `The customer has Comprehensive Coverage in the amount of ${finalAmount.pricingRuleSetAdjustmentAmount}, does this answer your question?`;
            return await confimIntent(session_attributes, intentName, message);
        }
        else {
            const message = "The customer does not have the discount or coverage you asked about on the policy selected. Do you need anything else or does that answer your question?";
            return await confimIntent(session_attributes, intentName, message);
        }
    } else {
        const message = "The customer does not have the discount or coverage you asked about on the policy selected. Do you need anything else or does that answer your question?";
        return await confimIntent(session_attributes, intentName, message);
    }

}

async function discountCoverage(intent_request) {
    const session_attributes = intent_request["sessionAttributes"];
    const intentName = intent_request['currentIntent']['name'];
    const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];
    if (confirmationStatus == 'None') {
        // check if it is fire or yearmake and model
        if (session_attributes["makeModelInputTranscript"]) {
            return await autoDiscountCoverage(intent_request);
        } else {
            return await fireDiscountCoverage(intent_request);
        }
    }
    else if (confirmationStatus == 'Denied') {
        const message = `Thank you, I am connecting you to a representative`;
        return close('Fulfilled', message, session_attributes);
    }
    else if (confirmationStatus == 'Confirmed') {
        const message = 'Gald to help';
        return close('Fulfilled', message, session_attributes);
    }
}

async function dispatch(intent_request) {
    console.log("====intent_request======", JSON.stringify(intent_request));

    const intent_name = intent_request['currentIntent']['name'];
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'customer_data') {
        return await customerData(intent_request);
    } else if (intent_name == 'auto_fire') {
        return await autofireData(intent_request);
    } else if (intent_name == 'yearmakemodel') {
        return await yeakMakeModel(intent_request);
    } else if (intent_name == 'discount_coverage') {
        return await discountCoverage(intent_request);
    } else if (intent_name == 'policyNumber') {
        return await policyNumber(intent_request);
    }

}

exports.handler = async (event) => {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
};
