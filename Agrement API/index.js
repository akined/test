'use strict';
const AWS = require('aws-sdk');
const fetch = require('node-fetch');
const KMS = new AWS.KMS();
const parser = require('fast-xml-parser');

const username = process.env.USERNAME;
const password = process.env.PASSWORD;
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const url = ' https://agreementsummary-env1.cfapps-71a.opr.test.statefarm.org/AgreementIndexInquiry-2.0/services/AgreementIndexInquiry';

async function decryption(encrypted) {
    try{
        let decryptData = await KMS.decrypt({ CiphertextBlob: Buffer.from(encrypted, 'base64') }).promise();
        return decryptData.Plaintext.toString('ascii');
    
    }catch(err){
        console.error("Exception while decryption", err);
        return err;
    }
}

async function getAgrmnt_IN_Data(clientId){
    try{
        const decryptUsername = await decryption(username);
        const decryptPassword = await decryption(password);
        const authencrypt = Buffer.from(decryptUsername + ":" + decryptPassword).toString("base64");
        console.log("==========encryptedAuth==============", authencrypt);
        const payload= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:agr="http://agreementindexinquiry.service/">'+

   '<soapenv:Header/>'+

   '<soapenv:Body>'+

      '<agr:retrieveInformationByParty>'+

         '<partyInputList>'+

            '<callingApp>QBDD</callingApp>'+

            '<latencyIntolerant>true</latencyIntolerant>'+

            `<partyIds>${clientId}</partyIds>`+

            '<prodLineCds>A</prodLineCds>'+

         '</partyInputList>'+

      '</agr:retrieveInformationByParty>'+

   '</soapenv:Body>'+

'</soapenv:Envelope>'
        const response= await fetch(url, {
           method: 'POST',
           headers: {
               'Content-Type' : 'application/xml',
               'Authorization': `Basic ${authencrypt}`,
               //'SOAPAction':' https://agreementsummary-env1.cfapps-71a.opr.test.statefarm.org/AgreementIndexInquiry-2.0/services/AgreementIndexInquiry'
           },
           body: payload,
        });
        console.log("Retreived data", response);
        const parseData= await response.text();
        console.log("parsed data", parseData);
        var jsonObj = parser.parse(parseData);
        return jsonObj;
    }catch (err){
        console.error("Error while retrieving data", err);
        return err;
    }
}


exports.handler = async (event) => {
    try{
        const clientId = event["ClientID"];
        const data = await getAgrmnt_IN_Data(clientId);
        return data;
        
    }catch(err){
        console.log("Err in Lambda function", err);
        return err;
    }
};
