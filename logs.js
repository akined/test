'use strict'
const AWS = require('aws-sdk');
var CloudWatchLogs = new AWS.CloudWatchLogs();

// ---------------------------------------------------- Main handler ------------------------------------------------- //

exports.handler = async function (event, context) {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        // const params = {
        //     destination: 'sf-conncen-test-usw2-podd-vaafeh-coudwatchtos3blbot', /* required */
        //     from: 1514745000000, /* required */
        //     logGroupName: 'uwpcbl-lex/sf-conncen-test-uwpcbl-bot-logs', /* required */
        //     to: 1586802600000, /* required */
        //     // destinationPrefix: 'STRING_VALUE'/,
        //     // logStreamNamePrefix: 'STRING_VALUE',
        //     taskName: 'ExportingCloudwatchLogsToS3'
        // };
        // const data = await CloudWatchLogs.createExportTask(params).promise();
        // return data;
        const params = {
            logGroupName: 'uwpcbl-lex/sf-conncen-test-uwpcbl-bot-logs',
            endTime: 1586802600000,
        };
        const data = await CloudWatchLogs.filterLogEvents(params).promise();
        console.log("=======data========", data);
        return data;
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
}
