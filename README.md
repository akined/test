'use strict'
const AWS = require('aws-sdk');
var s3 = new AWS.S3({});
exports.handler = async function (event, context) {
    try {
        const s3Data = await s3.getObject({
            Bucket: event.Records[0].s3.bucket.name,
            Key: event.Records[0].s3.object.key,
        }).promise();
        console.log({ s3Data })
        const bufferDataToString = s3Data.Body.toString('utf-8');
        console.log({ bufferDataToString })
        const sanitized = '[' + bufferDataToString.replace(/\}\s*\{/g, '},{') + ']';
        console.log({ sanitized })
        const s3BucketData = JSON.parse(sanitized);
        console.log({ s3BucketData })

        const claimsData = s3BucketData.map((data) => {
            const s3Obj = data.Script == "Claims" ? data : {};
            return s3Obj;
        });
        console.log({ claimsData })
        if (claimsData.length > 0) {
            const newArray = claimsData.filter(value => JSON.stringify(value) !== '{}');
            const params = {
                
                Body: JSON.stringify(newArray),
                Bucket: "testpodd",
                Key: event.Records[0].s3.object.key,
                ServerSideEncryption: "AES256"
            };
            console.log({ params })
            const uploadData = await s3.putObject(params).promise();
            console.log({ uploadData })

        }
        const pcData = s3BucketData.map((data) => {
            const s3Object = data.Script == "P&C" ? data : {};
            return s3Object;
        });
        console.log({ pcData })
        if (pcData.length > 0) {
            const newArray = pcData.filter(value => JSON.stringify(value) !== '{}');
            const params1 = {
                Body: JSON.stringify(newArray),
                Bucket: "pcpodd",
                Key: event.Records[0].s3.object.key,
                ServerSideEncryption: "AES256"
            };
            console.log({ params1 })
            const Data1 = await s3.putObject(params1).promise();
            console.log({ Data1 })

        }
        ;
    }

    catch (err) {
        console.log("err", err);
        return err;
    }
};


#Git lab credentials 
#Username: aki.nedunuru@gmail.com
#Pass:Aki@tison95