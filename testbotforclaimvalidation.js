'use strict'
const AWS = require('aws-sdk');

// ---------------------------------------------------- DynamoDb connection set up. ------------------------------------------ //
const dynamodb = new AWS.DynamoDB({});
const dynamoTable = 'sf-conncen-rsch-use1-podd-vaafeh-testbotdynamo';
let response_dynamo;
const descriptors = ['L', 'M', 'N', 'S', 'B', 'SS', 'NULL', 'NS', 'BS'];
const flattenDynamoDbResponse = (o) => {

    // flattens single property objects that have descriptors  
    for (let d of descriptors) {
        if (o.hasOwnProperty(d)) {
            return o[d];
        }
    }

    Object.keys(o).forEach((k) => {

        for (let d of descriptors) {
            if (o[k].hasOwnProperty(d)) {
                o[k] = o[k][d];
            }
        }
        if (Array.isArray(o[k])) {
            o[k] = o[k].map(e => flattenDynamoDbResponse(e))
        } else if (typeof o[k] === 'object') {
            o[k] = flattenDynamoDbResponse(o[k])
        }
    });

    return o;
}

async function get_response(number) {
    try {
        const params = {
            TableName: dynamoTable,

            FilterExpression: "#CustomerPhoneNumber = :from",
            ExpressionAttributeNames: {
                "#CustomerPhoneNumber": "CustomerPhoneNumber",
            },
            ExpressionAttributeValues: {
                ":from": {
                    S: number
                }
            }
        };
        console.log("========= params ========", params);

        const dynamodbResponse = await dynamodb.scan(params).promise();
        console.log("========dyanmoresponse========", dynamodbResponse);

        const intentResponse = {
            Response: dynamodbResponse,
        };
        console.log("The intent resopnse is :", intentResponse);

        return intentResponse;
    } catch (err) {
        console.log("Error occured while trying to retrieve response for intent: ", number);
        console.log(err);
        return err.response['Error']['Message'];
    }
}

// ---------------- Helpers to build responses which match the structure of the necessary dialog actions ----------------------- //


const get_slots = (intent_request) => intent_request['currentIntent']['slots'];

const confirmIntent = (message, intent_request = {}, slotDetails, session_attributes = {}) => {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intent_request['currentIntent']['name'],
            'message': message,
            slots: slotDetails,
        }
    };
    console.log("================confirmIntent dialogResponse===========", message);
    return dialogResponse;
}

const close = (fulfillment_state, message, session_attributes = {}) => {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': {
                'contentType': 'SSML',
                'content': message
            }
        }
    };
    console.log("===========close dialoge response============", dialogResponse);
    return dialogResponse;
}

const elicitSlot = (sessionAttributes, intentName, slots, slotToElicit, message) => {
    const dialogResponse = {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,
        },
    };
    console.log("===========elicitSlot dialoge response============", dialogResponse);

    return dialogResponse;
}

const build_validation_result = (is_valid, message_content) => {
    if (is_valid) {
        return {
            "isValid": is_valid,
        }
    } else {
        return {
            'isValid': is_valid,
            'message': { 'contentType': 'CustomPayload', 'content': message_content }
        }
    }
};

const validate_number_type = (number) => {
    if (number == null || isNaN(number)) {
        return build_validation_result(false,
            'Please enter a valid Phone number.');
    } else {
        return build_validation_result(true, '');
    }
};
''
const validate_date_type = (date) => {
    console.log("=========date==========", date);
    console.log("======new===date==========", new Date(date));
    console.log("=========date====string======", new Date(date).toString());
    if (new Date(date).toString() == 'Invalid Date') {
        return build_validation_result(false,
            'Please enter a valid Date.');
    } else {
        return build_validation_result(true, '');
    }
};


// ----------------------------------------- Functions that control the bot's behavior --------------------------------------- //

async function number(intent_request) {
    const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];
    const intentName = intent_request['currentIntent']['name'];
    const slots = { numbers: intent_request['currentIntent']['slots']['numbers'] };
    console.log("===========number intent invoked ============", confirmationStatus, intentName, slots);

    const validation_result = validate_number_type(intent_request['currentIntent']['slots']['numbers']);
    if (!validation_result['isValid']) {
        return elicitSlot({}, intentName, { numbers: null }, 'numbers', validation_result['message']);
    } else {
        if (confirmationStatus == 'None') {
            console.log("===========number None invoked ============");
            const message = {
                'contentType': 'SSML',
                'content': `<speak>The number you said is ${intent_request['currentIntent']['slots']['numbers']}, Is this correct?</speak>`
            };
            return confirmIntent(message, intent_request, slots, {});
        }
        else if (confirmationStatus == 'Denied') {
            console.log("===========number Denied invoked ============");

            const message = {
                'contentType': 'SSML',
                'content': 'Please say the number again'
            };
            return elicitSlot({}, intentName, { numbers: null }, 'numbers', message);
        }
        else if (confirmationStatus == 'Confirmed') {
            console.log("===========number  confirmed ============");
            const num = get_slots(intent_request)["numbers"];
            const CP = num.toString();
            console.log(CP, '===CP====')
            const cpLength = CP.length;
            let number;
            if (cpLength == 10) {
                number = `+1${num}`
            } else {
                number = num;
            }
            response_dynamo = await get_response(number); // repsonse from dynamodb
            let output;

            if (response_dynamo.Response.Count == 0) {
                output = '</speak>Sorry, I Did not found any claim regarding this phone number</speak>'
                return close('Fulfilled', output);
            }
            else if (response_dynamo.Response.Count == 1) {
                const singleClaimData = response_dynamo.Response.Items.map((item) => flattenDynamoDbResponse(item));
                console.log("==========singleClaimData============", singleClaimData);
                output = `I found a Claim on the date: ${singleClaimData[0].DateOfLoss} with claim number ${singleClaimData[0].ClaimNumber}. How Can I Help You?`;
                return close('Fulfilled', output);
            }
            else {
                const dynamodbFlattenedData = response_dynamo.Response.Items.map((item) => flattenDynamoDbResponse(item));
                const dates = dynamodbFlattenedData.map((item) => item.DateOfLoss);
                output = `I found Claims on these ${JSON.stringify(dates)} dates, please repeat a date that you need to know claim about`;
                const session_attributes = {
                    dates: JSON.stringify(dates),
                    number: num,
                    claimsData: JSON.stringify(dynamodbFlattenedData),
                };
                var message = output;
                return close('Fulfilled', message, session_attributes);
            }
        }
    }
}

async function claimNumber(intent_request) {
    console.log("========claimNumber=====invoked======", intent_request);
    const confirmationStatus = intent_request['currentIntent']['confirmationStatus'];
    const slots = { date: intent_request['currentIntent']['slots']['date'] };
    const session_attributes = intent_request['sessionAttributes'];
    const intentName = intent_request['currentIntent']['name'];

    const validation_result = validate_date_type(intent_request['currentIntent']['slots']['date']);
    if (!validation_result['isValid']) {
        return elicitSlot({}, intentName, slots, 'date', validation_result['message']);
    } else {
        if (confirmationStatus == 'None') {
            console.log("======claimNumber===None invoked===");
            const message = {
                'contentType': 'SSML',
                'content': `The date you said is ${intent_request['currentIntent']['slots']['date']}, Is this correct?`
            };
            return confirmIntent(message, intent_request, slots, session_attributes);
        } else if (confirmationStatus == 'Denied') {
            console.log("======claimNumber===Denied invoked===");
            const message = {
                'contentType': 'SSML',
                'content': 'Please repeat the date again'
            };
            return elicitSlot(session_attributes, intentName, intent_request['slots'], 'date', message);
        }
        else if (confirmationStatus == 'Confirmed') {
            console.log("======claimNumber===confirmed invoked===");
            const date = get_slots(intent_request)["date"];
            const parsedSessionAttributes = JSON.parse(session_attributes['claimsData']);
            console.log("======parsedSessionAttributes===confirmed invoked===", parsedSessionAttributes);
            const finalData = parsedSessionAttributes.find((item) => item.DateOfLoss == date);
            console.log("======finalData===confirmed invoked===", finalData);
            if (typeof finalData == 'undefined') {
                console.log("======finalData===undefined===");
                const output = `Sorry, I Did not found any claim regarding this date: ${date}`;
                return close('Fulfilled', output, session_attributes);
            } else {
                console.log("======finalData===not===");
                const output = `The claim number is ${finalData['ClaimNumber']}, How can i help you? `;
                return close('Fulfilled', output, session_attributes);
            }
        }
    }
}
// ------------------------------------------ Dispatch Intents ---------------------------------------------- //

async function dispatch(intent_request) {
    console.log("====intent_request======", intent_request);

    const intent_name = intent_request['currentIntent']['name']
    console.log('Intent {} is invoked.', intent_name);

    if (intent_name == 'Number') {
        return await number(intent_request);
    } else if (intent_name == 'Claim_Number') {
        return await claimNumber(intent_request);
    }
};

// ---------------------------------------------------- Main handler ------------------------------------------------- //

exports.handler = async function (event, context) {
    //Route the incoming request based on intent.
    //The JSON body of the request is provided in the event slot.
    try {
        return await dispatch(event);
    }
    catch (err) {
        console.log("Err In Lambda Function", err);
        return err;
    }
}
