'use strict'
const AWS = require('aws-sdk');
const csv = require('csvtojson');

var s3 = new AWS.S3({});


async function fetchAllTheItems() {
    const docClient = new AWS.DynamoDB.DocumentClient();
    const dbItems = await docClient.scan({ TableName: "BLTestTable" }).promise();
    console.log("===========dbITems============", dbItems, Array.isArray(dbItems));
    return dbItems;
}

async function callBatchWriteItemDynamo({ dynamoInstance, mappedData, }) {
    const params = {
        RequestItems: {
            'BLTestTable': mappedData
        },
    };
    console.log("mappedData", JSON.stringify(mappedData));
    // Call DynamoDB to add the item to the table
    const dynamoDBResponse = await dynamoInstance.batchWriteItem(params).promise();
    console.log("dynamoDBResponse", dynamoDBResponse);
    return dynamoDBResponse;
};

exports.handler = async function (event, context) {
    try {
        const s3Data = s3.getObject({
            Bucket: event.Records[0].s3.bucket.name,
            Key: event.Records[0].s3.object.key,
        }).createReadStream();

        const jsonData = await csv().fromStream(s3Data);
        console.log("jsondata", jsonData);

        var ddb = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

        // fetch all the data and delete all the rows with intentname key
        const dbItem = await fetchAllTheItems();
        if (dbItem.length > 0) {
            console.log("+=======Inside dbitem");
            const deleteReq = dbItem.map((data) => {
                const dynamoObj = {
                    DeleteRequest: {
                        Key: {
                            "IntentName": { S: data.IntentName },
                        }
                    }
                };
                return dynamoObj;
            });
            await callBatchWriteItemDynamo({ dynamoInstance: ddb, mappedData: deleteReq });
        }


        // for updating or inserting into the dynamodbtable

        const mappedDataForUpdateAndInsert = jsonData.map((data) => {
            const dynamoObj = {
                PutRequest: {
                    Item: JSON.parse(JSON.stringify({
                        "IntentName": { S: data.IntentName },
                        "EscalationSkillID": data.EscalationSkillID ? { S: data.EscalationSkillID } : undefined,
                        "EscalationSkillName": data.EscalationSkillName ? { S: data.EscalationSkillName } : undefined,
                        "FAQ": data.FAQ ? { S: data.FAQ } : undefined,
                        "FAQResponse": data.FAQResponse ? { S: data.FAQResponse } : undefined,
                        "Utterances": data.Utterances ? { S: data.Utterances } : undefined,
                    }))
                }
            };
            return dynamoObj;
        });
        return await callBatchWriteItemDynamo({ dynamoInstance: ddb, mappedData: mappedDataForUpdateAndInsert });
    } catch (err) {
        console.log("err", err);
        return err;
    }
};
