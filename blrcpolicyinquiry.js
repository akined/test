const intRes = require('./../utils/intentResponse');
const constants = require('./../utils/enums');

//For PT Status
async function aptPTStatus(intent_request) {
    let intentName = intent_request['currentIntent']['name'];
    let session_attributes = intent_request["sessionAttributes"];
    let userinfo = JSON.parse(session_attributes.blrcUserInfo);
    console.log(userinfo);
    let slots = intent_request['currentIntent']['slots'];
    let input = intent_request['inputTranscript'];
    console.log("inside aptPTStatus" + intent_request);

    if (input != 'Yes' && input != 'No' && input != 'Auto' && input != 'Fire') {
        console.log("inside aptPTStatusSelection1  yes is null " + intentName);
        let message = constants.displayMessages.blrcPTStatus1;
        session_attributes.aptPTStatusFlow = "AgentStatusQAsked";
        return intRes.confirmIntent(session_attributes, intentName, message, slots);
    }
    else if (input == 'Yes'
        && (session_attributes['aptPTStatusFlow'] == "AgentStatusQAsked"
            || session_attributes['aptPTStatusFlow'] == "AgentStatusLinkProvided")) {
        console.log("inside aptPTStatusSelection2 " + intentName);
        console.log("state code", userinfo.stateCode);
        //session_attributes.aptPTStatusFlow = "AutoOrFire";
        return intRes.elicitSlot(session_attributes, intentName, slots, 'autofire');

    }
    else if (input == 'Yes' &&
        (session_attributes['aptPTStatusFlow'] == "AgentStatusQAsked" ||
            session_attributes['aptPTStatusFlow'] == "AgentStatusLinkProvided")
        && ["21", "39"].includes(userinfo.stateCode)) {
        console.log("inside user state code 21 and 39 " + intentName);
        console.log("state code", userinfo.stateCode);
        //session_attributes.aptPTStatusFlow = "AutoOrFire";
        return intRes.queueIntent(intent_request);
    }

    else if (input == 'Auto' | input == 'Fire'
        && session_attributes['aptPTStatusFlow'] == "AgentStatusQAsked"
        || session_attributes['aptPTStatusFlow'] == "AgentStatusLinkProvided") {
        console.log("inside aptPTStatusSelection3 " + intentName);
        return intRes.queueIntent(intent_request);
    }

    else if (input == 'No' && session_attributes['aptPTStatusFlow'] != "AgentStatusLinkProvided") {
        console.log("inside aptPTRouteSelection5 " + intentName);
        console.log("inside aptPTRouteSelection5  " + input);

        let message = constants.displayMessages.blrcPTStatus2;
        session_attributes.aptPTStatusFlow = "AgentStatusLinkProvided";
        return intRes.confirmIntent(session_attributes, intentName, message, slots);

    }
    else if (input == 'No' && session_attributes['aptPTStatusFlow'] == "AgentStatusLinkProvided") {
        let message = constants.displayMessages.blrcGenericThankyou;
        return intRes.close('Fulfilled', message, session_attributes);
    }
    //  else if (input == 'Yes' && session_attributes['aptPTStatusFlow'] == "AgentStatusLinkProvided") {
    //     console.log("input Yes and session is equal attribute AgentStatusLinkProvided");
    //     return intRes.elicitSlot(session_attributes, intentName, slots, 'autofire');
    // }


}

module.exports = {
    aptPTStatus
};