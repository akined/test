const request = require('request');

module.exports = (jobId, token, callback) => {
    const options = {
        'method': 'GET',
        'url': `https://cignaproducer--dev.my.salesforce.com/services/data/v50.0/jobs/query/${jobId}`,
        'headers': { 'Authorization': `Bearer ${token}`, 'Cookie': 'BrowserId=Y7g2MgJTEeumb-GYWY0jHw' }
    };
    return request(options, function (error, response) {
        if (error) throw new Error(error); return callback(response.body)
    });
}; 