const AWS = require("aws-sdk");
const ssm = new AWS.SSM();
const fetch = require("node-fetch");
var s3 = new AWS.S3({});
const sns = new AWS.SNS({});
const salesforceLogin = require("/opt/login");
const salesforceQueryGetJobResults = require("/opt/query/getJobResults");
const salesforceWaitForJob = require('/opt/query/waitForJobStatus');
const csv = require('csvtojson');

const ssmName = process.env.SSM_NAME
const s3BucketName = process.env.BUCKET_NAME
const url = process.env.URL
const queryFromEnv = process.env.QUERY
const s3FileName = process.env.S3_FILENAME

//I reviewed bulk_fetch4 and would like you to make the following changes: 
// Start with these items first: • Rename Lambda function to community - cloud - analytic - extract 
//• The url variable needs to use an environment variable.This url will be different for each environment.
//• We need to use the user object for the query.Ajay will give you the final query.However, please use the following query 
//for now: o Select contactId, CBP_Email__c, Username, isActive, CreatedDate from user 
//• We need to write an SNS message after the file is persisted to S3.Here is the format: o 
//{
//  "table": "TODO", "domain": "sfcc-producer", "database": "sfcc_producer_raw", 
//"source_bucket_name": "da-datastore-sfcc-producer-raw.dev-cignasplithorizon", 
//"source_object_name": "sfcc-producer", "destination_bucket_name": "da-datastore-sfcc-producer.dev-cignasplithorizon", 
//"destination_object_name": "sfcc-producer", "refresh_type": "partitions_only", "refresh_date": "2020-10-1510:33:57",
//"partitioned_by": "'day'", "partition_sync": ["day=2020-10-15"] } 
//The source and destination bucket names should be passed in using environment variables.These items are lower priority but should also be done: • The SSM parameters look good except please see if you can use user1.• I also see that you are using a local s3 bucket.Please see if you can write to this one in the CDS account: s3://da-datastore-sfcc-producer-vendor.dev-cignasplithorizon • I would also like to see if you can leverage the libraries that Matt defined.

async function getUser() {
    const params = {
        Name: ssmName,
        WithDecryption: true,
    };
    return ssm.getParameter(params).promise();
}

async function createJob(token, finalQuery = {}) {
    console.log("=========createJob=======invoked===");
    const options = {
        method: "POST",
        headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ operation: "query", ...finalQuery }),
    };
    console.log("=========createJob=======options===", JSON.stringify(options));
    const data = await fetch(url, options);
    console.log("=========createJob=======data===", JSON.stringify(data));
    const finalResponse = data.json();
    console.log("=========createJob=======finalResponse===", JSON.stringify(finalResponse));
    return finalResponse;
}

async function getJobInfo(token, jobId) {
    console.log("=========getJobInfo=======invoked===");
    const options = {
        'method': 'GET',
        'url': `${url}/${jobId}`,
        'headers': {
            'Authorization': `Bearer ${token}`,
            'Cookie': 'BrowserId=Y7g2MgJTEeumb-GYWY0jHw'
        }
    };
    console.log("=========getJobInfo=======options===", JSON.stringify(options));
    const data = await fetch(url, options);
    console.log("=========getJobInfo=======data===", JSON.stringify(data));
    const finalResponse = data.json();
    console.log("=========getJobInfo=======finalResponse===", JSON.stringify(finalResponse));
    return finalResponse;
}

async function getJobResult(token, jobId) {
    console.log("=========getJobResult=======invoked===");
    const url = `${url}/${jobId}/results`;
    const options = {
        'method': 'GET',
        'headers': {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json',
            'Accept': 'test/csv',
            'Cookie': 'BrowserId=Y7g2MgJTEeumb-GYWY0jHw'
        }
    };
    console.log("=========getJobResult=======options===", options);
    const data = await fetch(url, options);
    console.log("=========getJobResult=======data===", JSON.stringify(data));
    const csvResponse = await data.text();
    console.log("======csvResponse==========", csvResponse, typeof csvResponse);
    const finalDataFromCsv = await csv().fromString(csvResponse);
    console.log("======finalDataFromCsv======", finalDataFromCsv);
    console.log("=========getJobResult=======finalResponse===", JSON.stringify(finalDataFromCsv));
    return finalDataFromCsv;
}

async function sendSns() {
    // const number = data['Item']['CONTACT_NUMBER'];
    const message = {
        "table": "TODO",
        "domain": "sfcc-producer",
        "database": "sfcc_producer_raw",
        "source_bucket_name": "da-datastore-sfcc-producer-raw.dev-cignasplithorizon",
        "source_object_name": "sfcc-producer",
        "destination_bucket_name": "da-datastore-sfcc-producer.dev-cignasplithorizon",
        "destination_object_name": "sfcc-producer",
        "refresh_type": "partitions_only",
        "refresh_date": "2020-10-1510:33:57",
        "partitioned_by": "'day'",
        "partition_sync": ["day=2020-10-15"]
    }
    const params = {
        Message: JSON.stringify(message),
        PhoneNumber: `+1${number}`,
        Subject: "SF HomePage"
    };
    console.log("=======params====", params);
    const snsResponse = await sns.publish(params).promise();
    console.log("=======snsResponse====", snsResponse);
    return snsResponse;
}

exports.handler = async (event) => {
    try {
        const ssmData = await getUser();
        const parsedSsmData = JSON.parse(ssmData['Parameter']['Value']);
        console.log("==========parsedSsmData=============", parsedSsmData);
        const salesForceLoginData = await salesforceLogin({
            "username": parsedSsmData['username'],
            "password": parsedSsmData['password'],
            "secretToken": parsedSsmData['secretToken'],
            "clientId": parsedSsmData['clientId'],
            "clientSecret": parsedSsmData['clientSecret']
        });
        const token = salesForceLoginData["access_token"];
        const createJobData = await createJob(token, {
            query: queryFromEnv,
            contentType: "CSV",
            lineEnding: "CRLF"
        });
        console.log("=========async=======createJobData===", typeof createJobData, JSON.stringify(createJobData));

        const jobId = createJobData["id"];
        const getJobInfoData = await getJobInfo(token, jobId);
        console.log("=========async=======getJobInfoData===", typeof getJobInfoData, JSON.stringify(getJobInfoData));

        // const getJobResultData = await getJobResult(token, jobId);

        const getJobResultData = await salesforceWaitForJob(jobId, token);
        console.log("=========async=======getJobResultData===", JSON.stringify(getJobResultData));

        const getJobResultData = await salesforceQueryGetJobResults(jobId, token);
        console.log("=========async=======getJobResultData===", typeof getJobResultData, JSON.stringify(getJobResultData));
        console.log("===parsed=====getJobResultData==============", JSON.parse(getJobResultData));

        // const snsResponse = await sendSns();
        // console.log("=========async=======snsResponse===", JSON.stringify(snsResponse));

        const params = {
            Body: JSON.stringify(getJobResultData),
            Bucket: s3BucketName,
            Key: s3FileName,
            ServerSideEncryption: "AES256"
        };
        console.log({ params });
        const s3Response = await s3.putObject(params).promise();
        console.log({ s3Response });

        const finalResponse = {
            statusCode: 200,
            body: JSON.stringify(salesForceLoginData)
        };

        return finalResponse;
    } catch (err) {
        console.log(err, err.stack);
        return { statusCode: 500, body: JSON.stringify(err) };
    }
}; 