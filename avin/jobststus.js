// This will wait for a job status to not be InProgress and return results
const fetch = require("node-fetch");
module.exports = (job, token) => {
    const url = `https://cignaproducer--dev.my.salesforce.com/services/data/v50.0/jobs/query/${job.id}`;
    const options = {
        method: "GET", headers: {
            Authorization: `Bearer ${token}`,
            Cookie: "BrowserId=Y7g2MgJTEeumb-GYWY0jHw",
        },
    };
    const waitForQuery = new Promise((resolve, reject) => {
        const timeout = 1000; // ms // Need to wait a second before we query after an upload 
        let update = 0; let maxUpdates = 10; let status;
        const salesforceAPICallInterval = setInterval(() => {
            console.log(update)
            update += 1; if (update >= maxUpdates) {
                clearInterval(salesforceAPICallInterval)
                console.log('took too long')
                return reject(`Job query took too long. Final status: ${status}`);
            }
            fetch(url, options).then((res) => {
                return res.json();
            }).then((jobStatus) => {
                status = jobStatus
                console.log("status.state", status.state)
                if (status.state !== 'InProgress') {
                    clearInterval(salesforceAPICallInterval)
                    job['status'] = jobStatus
                    return resolve(job);
                }
            }).catch((err) => {
                clearInterval(salesforceAPICallInterval);
                return reject(err);
            });
        },
            timeout);
    });
    return waitForQuery;
}; 