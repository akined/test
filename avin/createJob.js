const fetch = require("node-fetch");
module.exports = (query, token) => {
    const url = "https://cignaproducer--dev.my.salesforce.com/services/data/v50.0/jobs/query";
    const options = {
        method: "POST", headers: { Authorization: `Bearer ${token}`, "Content-Type": "application/json", },
        body: JSON.stringify({ operation: "query", query: query }),
    };
    return fetch(url, options).then((res => { return res.json(); }));
}; 