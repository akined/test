async function getCovDiscInfo(intentName, session_attributes, inputTranscript) {

    let strIntentName = intentName;
    // let strSession_attributes = session_attributes;
    let strInputText = inputTranscript;
    let custDtl = JSON.parse(session_attributes['CustomerDetails']);
    let sel = custDtl['Selection'];
    let agreSrcSysCd = sel['agreSrcSysCd'];
    let agreAccessKey = sel['agreAccessKey'];
    let intentType = sel['intentType'];
    let dvlServiceResponse = "";


    console.log("agreementSourceCd: " + agreSrcSysCd + "  agreementAccessKey:" + agreAccessKey + "  inputTranscript:" + strInputText);
    let strMetaData = constants.botIntentToDataMap[strIntentName];
    console.log("strMetaData " + JSON.stringify(strMetaData));

    let lambdaOutput = await invokeLambda(
        dvlServiceUrl, { agreAccessKey: agreAccessKey, agreSrcSysCd: agreSrcSysCd });
    dvlServiceResponse = JSON.parse(lambdaOutput.Payload);
    console.log("lambda Output idCardServiceResponse", dvlServiceResponse);
    let retMessage = `No ${intentType} Information found.`;
    let objDiscount = null;
    switch (strIntentName) {
        case "PCU_CI_BodilyInjury":
            objDiscount = dvlServiceResponse['policy']['termVersion']['insurableRisk'][0]['coverageSet']['coverage'];
            retMessage = strMetaData.notFoundMessage;
            const systemNameData = objDiscount.filter((item) => item[strMetaData.fieldName] == strMetaData.fieldNameValue);
            console.log("=========systemNameData=======", systemNameData);
            const systemNameLimitPerPerson = Array.isArray(systemNameData) && systemNameData[0][strMetaData.fieldArray2].filter(
                (item) => item[strMetaData.fieldName2] == strMetaData.fieldNameValue2);
            console.log("=========systemNameLimitPerPerson=======", systemNameLimitPerPerson);
            const value = Array.isArray(systemNameLimitPerPerson) && systemNameLimitPerPerson[0][strMetaData.fieldAmount];
            retMessage = `Y - The customer has the Accident Free Discount in the amount of $${value} per term`;

            //             BODILY_INJURY 
            // "PCU_CI_BodilyInjury" : {"fieldArray": "coverage", 
            // "fieldName": "systemName",
            // "fieldNameValue": "BODILY_INJURY", 
            // "fieldArray2":"coverageProperty",
            // "fieldName2": "systemName",
            // "fieldNameValue2": "LIMIT_PER_PERSON", 
            // "fieldAmount":"valueNum" ,
            // "notFoundMessage":"N - The customer does not have the Bodily Injury Coverage" ,

            // }, 
            // for (let coverage of objDiscount[strMetaData.fieldArray]) {
            //     console.log("coverage : " + strMetaData.fieldName + " -- " + coverage[strMetaData.fieldName]);
            //     let objDiscount1 = objDiscount[strMetaData.fieldArray.fieldArray2];
            //     for (let coverage of objDiscount1) {

            //         console.log("coverage : " + strMetaData.fieldName + " -- " + coverage[strMetaData.fieldName]);
            //         if (coverage[strMetaData.fieldName2] == strMetaData.fieldNameValue2) {


            //             retMessage = `Y - The customer has the Accident Free Discount in the amount of $${coverage[strMetaData.fieldAmount]} per term`;
            //             break;
            //         }
            //     }
            // }
            // return retMessage;
            return retMessage;

        case "PCU_CI_DriveSafeandSave":
            objDiscount = dvlServiceResponse['policy']['termVersion']['insurableRisk'][0]['pricingRuleSet'];
            retMessage = strMetaData.notFoundMessage;
            for (let discount of objDiscount[strMetaData.fieldArray]) {
                console.log("discount : " + strMetaData.fieldName + " -- " + discount[strMetaData.fieldName]);
                console.log("discount : " + strMetaData.fieldAmount + " -- " + discount[strMetaData.fieldAmount]);
                if (discount[strMetaData.fieldName] == strMetaData.fieldNameValue) {
                    retMessage = `Y - The customer has the Drive Safe and Save discount in the amount of $${discount[strMetaData.fieldAmount]} per term`;
                    break;
                }
            }
            return retMessage;

        default:
            return retMessage;

    }
}


async function getDataForSystemName(systemNameData, strMetaData, keyName) {
    let retMessage = constants.nodata.notfound;
    if (systemNameData.length > 0) {
        console.log("in if loop");
        console.log("in 2nd if block");
        const limitPerOccValue = Array.isArray(systemNameData) && systemNameData[0][strMetaData.fieldArray2].filter((item) =>
            item.systemName == 'LIMIT_PER_OCCURRENCE');
        const limitPerEmpleValue = Array.isArray(systemNameData) && systemNameData[0][strMetaData.fieldArray2].filter((item) =>
            item.systemName == 'LMT_PER_EMPLE');
        const limitPerPieceValue = Array.isArray(systemNameData) && systemNameData[0][strMetaData.fieldArray2].filter((item) =>
            item.systemName == 'LMT_PER_PIECE');

        console.log("limitPerOccValue", limitPerOccValue);
        console.log("limitPerEmpleValue", limitPerEmpleValue);
        console.log("limitPerPieceValue", limitPerPieceValue);

        retMessage = `The customer has ${keyName} coverage with a limit per occurrence of $${limitPerOccValue[0].valueNum}\n
                limit per employee of $${limitPerEmpleValue[0].valueNum}} and limit per piece of ${limitPerPieceValue[0].valueNum}`;
        return retMessage;
    }
    else {
        console.log("in else loop");
        return retMessage;
    }
}

async function getCoverageData(coverageData, strMetaData) {

    // let response = dvlServiceResponse['policy']['termVersion']['insurableRisk'][0]['coverageSet']['coverage'];
    console.log("response", coverageData);

    const offPremiseMetaData = coverageData.filter((item) => item[strMetaData.fieldName] == 'EMPLE_EQP_OFF_PREMISES');
    const onPremiseMetaData = coverageData.filter((item) => item[strMetaData.fieldName] == 'EMPLE_EQP_ON_PREMISES');
    console.log("=========offPremiseMetaData=======", offPremiseMetaData);
    console.log("=========onPremiseMetaData=======", onPremiseMetaData);

    const offPremiseData = await getDataForSystemName(offPremiseMetaData, strMetaData, 'EMPLE_EQP_OFF_PREMISES');
    const onPremiseData = await getDataForSystemName(onPremiseMetaData, strMetaData, 'EMPLE_EQP_ON_PREMISES');

    console.log("=========offPremiseMetaData==final data=====", offPremiseData);
    console.log("=========onPremiseMetaData== final data=====", onPremiseData);

    return `${offPremiseData}. ${onPremiseData}`;
}

