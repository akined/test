const utils = require('./util');

const close = (fulfillment_state, message, session_attributes = {}) => {
    const dialogResponse = {
        sessionAttributes: session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': {
                'contentType': 'PlainText',
                'content': message
            }
        }
    };
    console.log("===========close dialoge response============", dialogResponse);
    return dialogResponse;
};

const elicitSlot = (sessionAttributes, intentName, slots, slotToElicit, message) => {
    const dialogResponse = {
        sessionAttributes,
        dialogAction: {
            type: 'ElicitSlot',
            intentName,
            slots,
            slotToElicit,
            message,
            'responseCard': {
                'version': 1,
                'contentType': 'application/vnd.amazonaws.card.generic',
                'genericAttachments': [{
                    'buttons': [
                        { text: 'Auto', value: 'Auto' },
                        { text: 'Fire', value: 'Fire' },
                    ]
                }]
            }
        },
    };
    console.log("===========elicitSlot dialoge response============", dialogResponse);
    return dialogResponse;
};


const confirmIntent = (session_attributes, intentName, message, slots) => {
    const dialogResponse = {
        'sessionAttributes': session_attributes,
        //'requestAttributes':request_attributes,
        'dialogAction': {
            slots,
            'type': 'ConfirmIntent',
            'intentName': intentName,
            'message': {
                'contentType': 'CustomPayload',
                'content': message
            },
            'responseCard': {
                'version': 1,
                'contentType': 'application/vnd.amazonaws.card.generic',
                'genericAttachments': [{
                    'buttons': [
                        { text: 'Yes', value: 'Yes' },
                        { text: 'No', value: 'No' },
                    ]
                }]
            }
        }
    };
    console.log("The dialogResponse will be formatted as : ", dialogResponse);
    return dialogResponse;
};


async function queueIntent(intent_request) {
    let filteredData;
    const session_attributes = intent_request["sessionAttributes"];
    const displayBtns = JSON.parse(session_attributes['rtnButtons']);
    const lob = intent_request['currentIntent']['slots']['autofire'];
    console.log("displayBtns", displayBtns);
    // const stateCode = JSON.parse(session_attributes.blrcUserInfo.stateCode);

    if (lob) {
        filteredData = displayBtns.filter((data) => data["lob"].toUpperCase() == lob.toUpperCase());

    } else {
        filteredData = displayBtns;
    }

    console.log("filteredData", filteredData);
    const buttons = filteredData.map((data) => ({
        text: `${data.label} `,
        value: `${data.label}`
    }));


    const dialogResponse = {
        //   'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': {
                'contentType': 'CustomPayload',
                'content': "Please click on a line of business :"
            },
            'responseCard': {
                'version': 1,
                'contentType': 'application/vnd.amazonaws.card.generic',
                'genericAttachments': [{
                    'title': '',
                    'buttons': buttons
                }]
            }
        }
    };
    console.log("================routeToLobQueue dialogResponse===========", JSON.stringify(dialogResponse));
    return dialogResponse;
}

//Queue routing
async function queueRouting(intent_request) {
    let intentName = intent_request['currentIntent']['name'];
    let session_attributes = intent_request["sessionAttributes"];
    let slots = intent_request['currentIntent']['slots'];
    let input = intent_request['inputTranscript'];

    //intent_request['currentIntent']['name'] = intents.botIntents.int_othertopics;
    //console.log("inside queueRouting " + intentName);
    //console.log("inside queueRouting if condition  " + input);

    let varSkillId = await utils.getRoutingSkillID(session_attributes, input);
    session_attributes['adapterAction'] = "Transfer";
    session_attributes['skillId'] = varSkillId;
    let message = '.';
    return close('Fulfilled', message, session_attributes);

}
async function initialButtons(intentName, sessionAttributes, rtMetaData) {
    const dialogResponse = {
        sessionAttributes,
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': {
                'contentType': 'CustomPayload',
                'content': "What is the Topic?"
            },

        }
    };
    const responseCard = {
        'version': 1,
        'contentType': 'application/vnd.amazonaws.card.generic',
        'genericAttachments': []
    };

    let filteredData = rtMetaData.initialMenu;
    console.log("filteredData before sorting", filteredData);
    filteredData.sort((a, b) => (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0));
    console.log("filteredData after sorting", filteredData);
    
    const buttons = filteredData.map((data) => ({
        text: `${data.label} `,
        value: `${data.value}`
    }));


    // Create the generic attachment to add to the lex response.
    const attachment = {
        'buttons': buttons
    };

    responseCard['genericAttachments'].push(attachment);
    dialogResponse['dialogAction']['responseCard'] = responseCard;
    console.log("dialogResponse", JSON.stringify(dialogResponse));
    return dialogResponse;
}



module.exports = {
    close,
    elicitSlot,
    confirmIntent,
    queueIntent,
    queueRouting
};